import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

verifDisplay = [
	'Object-spy/Home_Page/Login_Button',
	'Object-spy/Home_Page/categories',
	'Object-spy/Home_Page/Feature Products',
	'Object-spy/Home_Page/Logo',
	'Object-spy/Home_Page/navbar',
	'Object-spy/Home_Page/Register_Button',
	'Object-spy/Home_Page/Right Menu',
	'Object-spy/Home_Page/Search_bar_store',
	'Object-spy/Home_Page/Shopping cart Button',
	'Object-spy/Home_Page/Side Menu',
	'Object-spy/Home_Page/Wishlist Button',
	'Object-spy/Home_Page/Footer_Section',
	]

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

lng = verifDisplay.size()

for (i = 0; i < lng; i++) {

	WebUI.verifyElementVisible(findTestObject(verifDisplay[i]))
	
}


WebUI.closeBrowser()

