import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

searchVar = ['book','computer','blue','not found','200']
verifSearch = [
	'Object-spy/Div_Search/div_book',
	'Object-spy/Div_Search/div_computer',
	'Object-spy/Div_Search/div_Blue_Sneaker',
	'Object-spy/Div_Search/div_not_found_search',
	'Object-spy/Div_Search/div_not_found_search'
	]

lng = searchVar.size()
	
WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.waitForPageLoad(0)

for(i=0;i<lng;i++) {
	
	WebUI.setText(findTestObject('Object-spy/Home_Page/Search_bar_store'), searchVar[i])
	
	WebUI.click(findTestObject('Object-spy/Home_Page/Search_button'))
	
	WebUI.verifyElementVisible(findTestObject(verifSearch[i]))
	
}

WebUI.closeBrowser()

