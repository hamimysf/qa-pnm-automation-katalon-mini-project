import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

ObjectFooterVar  = [
	'Object-spy/Footer/Footer_Link_About us',
	'Object-spy/Footer/Footer_Link_Blog',
	'Object-spy/Footer/Footer_Link_Compare products list',
	'Object-spy/Footer/Footer_Link_Conditions of Use',
	'Object-spy/Footer/Footer_Link_Contact us',
	'Object-spy/Footer/Footer_Link_New products',
	'Object-spy/Footer/Footer_Link_News',
	'Object-spy/Footer/Footer_Link_Privacy Notice',
	'Object-spy/Footer/Footer_Link_Sitemap',
	'Object-spy/Footer/Footer_Link_Search',
	'Object-spy/Footer/Footer_Link_Recently viewed products',
	'Object-spy/Footer/Footer_Link_Shipping_Returns'
	]

ObjectVerifFooterVar = [
	'Object-spy/Footer/Section_verified/About_Us_Section',
	'Object-spy/Footer/Section_verified/Blog_Section',
	'Object-spy/Footer/Section_verified/Compare_Products',
	'Object-spy/Footer/Section_verified/Condition_Of_Use_Section',
	'Object-spy/Footer/Section_verified/Contact_Us_Section',
	'Object-spy/Footer/Section_verified/New_Products_Section',
	'Object-spy/Footer/Section_verified/News_Section',
	'Object-spy/Footer/Section_verified/Privacy_Policy_Section',
	'Object-spy/Footer/Section_verified/Sitemap_Section',
	'Object-spy/Footer/Section_verified/Search_Section',
	'Object-spy/Footer/Section_verified/Recently_View_Products_Section',
	'Object-spy/Footer/Section_verified/Shipping_return_Section'
	]

lng = ObjectFooterVar.size()
	
WebUI.openBrowser('')

	
for(i=0;i<lng;i++) {
	
	WebUI.navigateToUrl('https://demowebshop.tricentis.com/')
	
	WebUI.scrollToElement(findTestObject(ObjectFooterVar[i]), 2)
	
	WebUI.click(findTestObject(ObjectFooterVar[i]))
	
	WebUI.verifyElementVisible(findTestObject(ObjectVerifFooterVar[i]))
	
}



WebUI.closeBrowser()

