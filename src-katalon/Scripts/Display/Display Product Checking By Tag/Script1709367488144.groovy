import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

tags = [
	'Object-spy/Home_Page/Tags/tag_apparel',
	'Object-spy/Home_Page/Tags/tag_awesome',
	'Object-spy/Home_Page/Tags/tag_book',
	'Object-spy/Home_Page/Tags/tag_camera',
	'Object-spy/Home_Page/Tags/tag_cell',
	'Object-spy/Home_Page/Tags/tag_compact',
	'Object-spy/Home_Page/Tags/tag_computer',
	'Object-spy/Home_Page/Tags/tag_cool',
	'Object-spy/Home_Page/Tags/tag_digital',
	'Object-spy/Home_Page/Tags/tag_gift',
	'Object-spy/Home_Page/Tags/tag_jewelry',
	'Object-spy/Home_Page/Tags/tag_nice',
	'Object-spy/Home_Page/Tags/tag_shirt',
	'Object-spy/Home_Page/Tags/tag_shoes',
	'Object-spy/Home_Page/Tags/tag_View all'
	]

verifProduct = [
	'Object-spy/Div_Search/div_Blue_Sneaker',
	'Object-spy/Div_Search/div_Handycam',
	'Object-spy/Div_Search/div_book',
	'Object-spy/Div_Search/div_Handycam',
	'Object-spy/Div_Search/div_Smartphone',
	'Object-spy/Div_Search/div_Smartphone',
	'Object-spy/Div_Search/div_Laptop',
	'Object-spy/Div_Search/div_Blue_Sneaker',
	'Object-spy/Div_Search/div_Album_download',
	'Object-spy/Div_Search/div_giftcard',
	'Object-spy/Div_Search/div_jewelry',
	'Object-spy/Div_Search/div_giftcard',
	'Object-spy/Div_Search/div_Shirt',
	'Object-spy/Div_Search/div_Blue_Sneaker',
	'Object-spy/Div_Search/div_All_product'
	]

lng = tags.size()


WebUI.waitForPageLoad(0)

for (i = 0; i < lng; i++) {

	WebUI.navigateToUrl('https://demowebshop.tricentis.com/')
	WebUI.click(findTestObject(tags[i]))

	WebUI.verifyElementVisible(findTestObject(verifProduct[i]))
}

WebUI.closeBrowser()