import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

navVar = [
	'Object-spy/Home_Page/Navbar/nav_Books',
	'Object-spy/Home_Page/Navbar/nav_Computers',
	'Object-spy/Home_Page/Navbar/nav_Apparel  Shoes',
	'Object-spy/Home_Page/Navbar/nav_Digital downloads',
	'Object-spy/Home_Page/Navbar/nav_Electronics',
	'Object-spy/Home_Page/Navbar/nav_Gift Cards',
	'Object-spy/Home_Page/Navbar/nav_Jewelry'
	]

verifNavProduct = [
	'Object-spy/Div_Search/div_book', 
	'Object-spy/Div_Search/div_Desktops', 
	'Object-spy/Div_Search/div_Blue_Sneaker', 
	'Object-spy/Div_Search/div_Album_download', 
	'Object-spy/Div_Search/div_camera',
	'Object-spy/Div_Search/div_giftcard',
	'Object-spy/Div_Search/div_jewelry'
	]

lng = navVar.size()

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.waitForPageLoad(0)

for (i = 0; i < lng; i++) {
    WebUI.click(findTestObject(navVar[i]))

    WebUI.verifyElementVisible(findTestObject(verifNavProduct[i]))
}

WebUI.closeBrowser()

