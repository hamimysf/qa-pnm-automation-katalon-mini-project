import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('Object-spy/Home_Page/Register_Button'))

WebUI.waitForElementVisible(findTestObject('Object-spy/Page_Register/input__register-button'), 2)

WebUI.click(findTestObject('Object-spy/Register Page/input_male_gender'))

WebUI.setText(findTestObject('Object-spy/Register Page/input_firstname'), firstname)

WebUI.setText(findTestObject('Object-spy/Register Page/input_lastname'), lastname)

WebUI.setText(findTestObject('Object-spy/Register Page/input_email'), email)

WebUI.setText(findTestObject('Object-spy/Register Page/input_password'), password)

WebUI.setText(findTestObject('Object-spy/Register Page/confirm_password'), confirmpwd)

WebUI.click(findTestObject('Object-spy/Register Page/submit_register'))

WebUI.verifyElementVisible(findTestObject('Object-spy/Register Page/div_registration_completed'))

WebUI.click(findTestObject('Object-spy/Register Page/continue_button'))

WebUI.closeBrowser()

