import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('Object-spy/Home_Page/Login_Button'))

WebUI.waitForPageLoad(2)

WebUI.setText(findTestObject('Object-spy/Login Page/input email'), 'hamim_pnm@gmail.com')

WebUI.setText(findTestObject('Object-spy/Login Page/input password'), 'hamim123')

WebUI.click(findTestObject('Object-spy/Login Page/Login Buuton'))

WebUI.waitForPageLoad(0)

WebUI.click(findTestObject('Object-spy/Home_Page/user_link'))

WebUI.click(findTestObject('Object-spy/Address_page/link_address'))

WebUI.click(findTestObject('Object-spy/Address_page/edit_button'))

WebUI.setText(findTestObject('Object-spy/Address_page/firstname'), 'hamim')

WebUI.setText(findTestObject('Object-spy/Address_page/lastname'), 'yusuf')

WebUI.setText(findTestObject('Object-spy/Address_page/email'), 'hamim_pnm@gmail.com')

WebUI.setText(findTestObject('Object-spy/Address_page/company'), 'PNM')

WebUI.selectOptionByIndex(findTestObject('Object-spy/Address_page/select_country'), '1', FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByIndex(findTestObject('Object-spy/Address_page/province'), '1', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object-spy/Address_page/city'), 'Michigan')

WebUI.setText(findTestObject('Object-spy/Address_page/address1'), 'ST. LOUIS')

WebUI.setText(findTestObject('Object-spy/Address_page/address2'), 'Mount Hall')

WebUI.setText(findTestObject('Object-spy/Address_page/zipcode'), '768231')

WebUI.setText(findTestObject('Object-spy/Address_page/phone_number'), '12345678910')

WebUI.click(findTestObject('Object-spy/Address_page/save'))

WebUI.verifyTextPresent('PNM', false)

WebUI.verifyTextPresent('12345678910', false)

WebUI.verifyTextPresent('768231', false)

WebUI.verifyTextPresent('Mount Hall', false)

WebUI.verifyTextPresent('ST. LOUIS', false)

WebUI.verifyTextPresent('Michigan', false)

WebUI.closeBrowser()

