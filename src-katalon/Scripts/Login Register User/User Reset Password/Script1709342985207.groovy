import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.waitForPageLoad(2)

WebUI.click(findTestObject('Object-spy/Home_Page/Login_Button'))

WebUI.waitForPageLoad(2)

WebUI.click(findTestObject('Object-spy/Login Page/link_Forgot password'))

WebUI.waitForElementVisible(findTestObject('Object-spy/Password Recovery/div_recovery_password'), 0)

WebUI.setText(findTestObject('Object-spy/Password Recovery/input_email_to_recover'), email)

WebUI.click(findTestObject('Object-spy/Password Recovery/submit_recovery_email'))

WebUI.verifyElementVisible(findTestObject('Object-spy/Password Recovery/recover_password_send_to_email'), FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

