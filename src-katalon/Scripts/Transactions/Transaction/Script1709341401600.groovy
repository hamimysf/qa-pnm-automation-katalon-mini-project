import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.waitForPageLoad(2)

WebUI.click(findTestObject('Object-spy/Home_Page/Shopping cart Button'))

WebUI.verifyElementVisible(findTestObject('Object-spy/Shopping_Cart/div_empty_cart'))

WebUI.click(findTestObject('Object-spy/Home_Page/Login_Button'))

WebUI.waitForPageLoad(2)

WebUI.setText(findTestObject('Object-spy/Login Page/input email'), 'hamim_pnm@gmail.com')

WebUI.setText(findTestObject('Object-spy/Login Page/input password'), 'hamim123')

WebUI.click(findTestObject('Object-spy/Login Page/Login Buuton'))

WebUI.setText(findTestObject('Object-spy/Home_Page/Search_bar_store'), 'book')

WebUI.click(findTestObject('Object-spy/Home_Page/Search_button'))

WebUI.click(findTestObject('Object-spy/cart_button/book_cart'))

WebUI.click(findTestObject('Object-spy/Home_Page/Shopping cart Button'))

WebUI.waitForPageLoad(0)

WebUI.click(findTestObject('Object-spy/Cart/agree_checkout'))

WebUI.click(findTestObject('Object-spy/Cart/button_Checkout'))

WebUI.click(findTestObject('Object-spy/checkout_page/continue_billing'))

WebUI.click(findTestObject('Object-spy/checkout_page/check_pickup'))

WebUI.click(findTestObject('Object-spy/checkout_page/continue_shipping_address'))

WebUI.click(findTestObject('Object-spy/checkout_page/continue_payment'))

WebUI.click(findTestObject('Object-spy/checkout_page/continue_payment_info'))

WebUI.click(findTestObject('Object-spy/checkout_page/confirm_order'))

WebUI.verifyElementVisible(findTestObject('Object-spy/checkout_page/strong_Your order has been successfully processed'))

WebUI.click(findTestObject('Object-spy/checkout_page/order_info_link'))

WebUI.verifyElementVisible(findTestObject('Object-spy/order_detail/div_order_detail'))

WebUI.closeBrowser()

