<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>continue_shipping_address</name>
   <tag></tag>
   <elementGuidId>ec6e5da7-f773-44d1-8158-5c3a213aab0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value='Continue'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#shipping-buttons-container > input.button-1.new-address-next-step-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9f7912d2-1099-4b67-b919-d9eefcac79a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>45774a14-f69b-480c-9f66-9181306b7c06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 new-address-next-step-button</value>
      <webElementGuid>4e3e3e11-24a4-4f16-a6fb-0cd3d6e89630</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>df505cdb-13b4-4ac6-8abd-cc5c69350a0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>Shipping.save()</value>
      <webElementGuid>7385fb4f-f8b9-4ad9-915a-334708c34d36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>85b6189b-552d-4222-ac3b-6b7f7bfe40ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shipping-buttons-container&quot;)/input[@class=&quot;button-1 new-address-next-step-button&quot;]</value>
      <webElementGuid>710c490f-6134-4774-9f17-4271bffae522</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value='Continue'])[2]</value>
      <webElementGuid>caf219e5-8e00-4561-8ed1-358f683c4a98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shipping-buttons-container']/input</value>
      <webElementGuid>42c0a78d-a5bd-4594-aa1b-c2224c862af5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div[2]/div/input</value>
      <webElementGuid>a1d94399-f727-4828-9450-a20b38b4874d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button' and @title = 'Continue']</value>
      <webElementGuid>ddfb450a-4e7d-40f4-9df3-636821b23610</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
