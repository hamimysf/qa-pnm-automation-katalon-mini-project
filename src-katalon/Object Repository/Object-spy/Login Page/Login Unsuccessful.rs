<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Login Unsuccessful</name>
   <tag></tag>
   <elementGuidId>f6f5340d-6447-4c09-953a-995ad9983d9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8ecf1dbb-c41b-4bdd-a146-a2085c70f4e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login was unsuccessful. Please correct the errors and try again.</value>
      <webElementGuid>c38543e1-dca9-4f72-bede-13a44e396640</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/span[1]</value>
      <webElementGuid>cc329394-b918-4f12-8afd-395c018a51f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::span[1]</value>
      <webElementGuid>f72d26f4-901d-4815-b73b-e1973e1d1ce2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::span[1]</value>
      <webElementGuid>1bb24f91-7329-4831-aa20-544dc40c9f74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No customer account found'])[1]/preceding::span[1]</value>
      <webElementGuid>9463d041-af0e-4215-ad8b-a2df7149ba7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email:'])[1]/preceding::span[1]</value>
      <webElementGuid>cfb17101-8e48-47d9-85db-7af0dddade2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Login was unsuccessful. Please correct the errors and try again.']/parent::*</value>
      <webElementGuid>20c3e8b1-832a-40f8-a547-2dbb514f0dd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/span</value>
      <webElementGuid>5b3b7733-eb9d-4978-8f3c-524e72e5af1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Login was unsuccessful. Please correct the errors and try again.' or . = 'Login was unsuccessful. Please correct the errors and try again.')]</value>
      <webElementGuid>465cf72c-76f8-4c27-80e0-ffa77e4de910</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
