<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>No account found</name>
   <tag></tag>
   <elementGuidId>69d35ac6-bcdd-4dca-b90c-7a9c4a229769</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Login was unsuccessful. Please correct the errors and try again.'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > ul > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>b530e9c8-eee9-4cd8-b0f3-38c5a76435db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No customer account found</value>
      <webElementGuid>a8775252-a2ba-4e01-97dc-8de1128fdcf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/ul[1]/li[1]</value>
      <webElementGuid>0516461e-ab09-4b6d-96a1-cb480bbd83fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login was unsuccessful. Please correct the errors and try again.'])[1]/following::li[1]</value>
      <webElementGuid>9732c283-1234-4828-9113-412b162ee9f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::li[1]</value>
      <webElementGuid>d06e8f94-7520-4b4e-8fbd-8e7278db59ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email:'])[1]/preceding::li[1]</value>
      <webElementGuid>e58ad799-ff98-459a-b764-2e6fd08d802c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/preceding::li[1]</value>
      <webElementGuid>13c8697f-ee68-4032-80d4-3fca8a7b9ba9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='No customer account found']/parent::*</value>
      <webElementGuid>0b698f53-0ed7-4a24-8a3e-bd09d8e5a28f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/ul/li</value>
      <webElementGuid>9a59968d-1f1f-4eab-ba3b-421bf11f6c4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'No customer account found' or . = 'No customer account found')]</value>
      <webElementGuid>f62fb350-e141-43ed-9440-a88fa7c233a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
