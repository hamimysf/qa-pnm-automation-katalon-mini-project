<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_empty_cart</name>
   <tag></tag>
   <elementGuidId>77530531-f8df-4dcc-89fc-870783f1f998</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.order-summary-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>75c69adc-5897-4324-8b0c-b3caff3f088e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>order-summary-content</value>
      <webElementGuid>6d131d6d-858e-49e3-adae-4426ed0cc29e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
Your Shopping Cart is empty!    
</value>
      <webElementGuid>7f81c383-df6d-4bc2-bedc-4be7111714de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]</value>
      <webElementGuid>972d8d15-8ee6-4748-b1cb-dd21efc7c488</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[2]/following::div[2]</value>
      <webElementGuid>13017428-c1be-4830-b6aa-08e2c042bd5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete'])[1]/following::div[4]</value>
      <webElementGuid>369b411b-eb97-4934-8940-c89e10d53dc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[1]</value>
      <webElementGuid>23cf3a02-1e20-4365-ba02-de070a7cb285</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::div[1]</value>
      <webElementGuid>efee620a-6494-48e7-807d-2a12ce1db510</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your Shopping Cart is empty!']/parent::*</value>
      <webElementGuid>c427d9c4-cd95-4b0f-b430-548fcc0dac1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/div[2]/div</value>
      <webElementGuid>85c91389-c73b-4736-aa09-60b8db6d8a4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    
Your Shopping Cart is empty!    
' or . = '
    
    
Your Shopping Cart is empty!    
')]</value>
      <webElementGuid>501d8af1-148f-416f-9379-d3afafd006fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
