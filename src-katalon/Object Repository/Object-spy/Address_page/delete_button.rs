<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>delete_button</name>
   <tag></tag>
   <elementGuidId>f193e276-cffa-492c-b7c8-3314b5d31be6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Delete']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.button-2.delete-address-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fc4504a3-4c78-4411-9eb5-7977bbd47ebb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>02c30616-a3ae-4a45-9a42-85459e091bad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-2 delete-address-button</value>
      <webElementGuid>5be4f0dc-fd01-48ab-95c5-efe2e086b420</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>if (confirm('Are you sure?')) {location.href='/customer/addressdelete/3499483';}</value>
      <webElementGuid>a1fe1095-7e18-4a4c-8670-8e3f048a5a2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Delete</value>
      <webElementGuid>94c4d5e2-a0c5-4561-8aec-f46f495c1538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page account-page address-list-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;address-list&quot;]/div[@class=&quot;section address-item&quot;]/div[@class=&quot;buttons&quot;]/input[@class=&quot;button-2 delete-address-button&quot;]</value>
      <webElementGuid>f4fb58f9-368a-4c2b-b7f3-15c6c7b44920</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Delete']</value>
      <webElementGuid>d0a53907-241c-4852-b1ae-9f9a16b4fa53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input[2]</value>
      <webElementGuid>332b72fe-03c2-4f3f-a715-91b62f3e8010</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button']</value>
      <webElementGuid>ba336b82-111f-4d12-b452-380ac9c409b8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
