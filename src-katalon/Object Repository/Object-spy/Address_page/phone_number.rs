<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>phone_number</name>
   <tag></tag>
   <elementGuidId>726f7f38-3319-4c60-8ed1-49d7c576687e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Address_PhoneNumber']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Address_PhoneNumber</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e35a45a5-e716-4c02-af6e-732f78b5f883</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>7835a077-b798-4bd1-85a9-ecb598eaa4a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5c22a11d-997e-450d-9260-1870daa0abc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Phone is required</value>
      <webElementGuid>2e15e07d-838c-40ba-b485-44f6c50cee80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Address_PhoneNumber</value>
      <webElementGuid>30eafc50-cc66-48d7-9fd2-722d5734422e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Address.PhoneNumber</value>
      <webElementGuid>cfd86f72-6b4b-424f-b774-4b02362b3d65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>111f5b8c-f271-4f4c-b052-29619e7422f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1908799877263</value>
      <webElementGuid>00e26be3-a62c-46f8-9d6a-5efde22d2906</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Address_PhoneNumber&quot;)</value>
      <webElementGuid>d9f364e5-7939-445b-839f-ec7ddac4abdb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Address_PhoneNumber']</value>
      <webElementGuid>d9112eaa-e56e-44a5-bae7-8713e595479c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/input</value>
      <webElementGuid>5d2673e9-3127-4082-af24-2b1df2be025c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Address_PhoneNumber' and @name = 'Address.PhoneNumber' and @type = 'text']</value>
      <webElementGuid>fcfe39fc-1c4e-4cbf-9904-3bafb4d0ee3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
