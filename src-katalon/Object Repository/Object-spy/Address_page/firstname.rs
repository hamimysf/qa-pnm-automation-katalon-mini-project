<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>firstname</name>
   <tag></tag>
   <elementGuidId>9e7d6559-9eae-4275-abd6-748cbb176a01</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Address_FirstName']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Address_FirstName</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2ce83f7a-75b0-411d-a19c-d2d400fdeea3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>e28e6272-5fa3-433d-93e1-d9a0e797d383</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7155a91e-74be-4359-bd37-18941517230e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>First name is required.</value>
      <webElementGuid>ee636df8-74ed-47a7-992a-b6b1b053185f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Address_FirstName</value>
      <webElementGuid>8467f8b4-83b8-4b6d-bf06-2f02e5ab91fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Address.FirstName</value>
      <webElementGuid>139b374d-ca21-4aad-a896-2786d59fbd7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>be96b393-d36f-4f1c-92e8-c0e167fb4c76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Hamim</value>
      <webElementGuid>a07f39f6-4f9f-4c69-8eef-06a38fb7de16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Address_FirstName&quot;)</value>
      <webElementGuid>f99e4f8f-3ec0-47e6-910e-f24780334563</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Address_FirstName']</value>
      <webElementGuid>4ece4e8a-1465-4f53-9326-4cb074deb9c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/input</value>
      <webElementGuid>7c235098-1070-490a-9307-2f7918c14965</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Address_FirstName' and @name = 'Address.FirstName' and @type = 'text']</value>
      <webElementGuid>a822c2d8-aab4-4ffa-82d1-b678f12359ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
