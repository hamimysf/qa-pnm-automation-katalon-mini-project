<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_All_product</name>
   <tag></tag>
   <elementGuidId>25c2fdb9-98dd-4f86-bbbd-8af417f857c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.page.product-tags-all-page</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d918c807-4358-4cbd-bd10-938dc1f0f6dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page product-tags-all-page</value>
      <webElementGuid>e5c8c7ef-60d0-470d-b076-4b75ceb6865e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        All product tags
    
    
            
                    
                        apparel 
                    
                        awesome 
                    
                        book 
                    
                        camera 
                    
                        cell 
                    
                        compact 
                    
                        computer 
                    
                        cool 
                    
                        cover 
                    
                        digital 
                    
                        game 
                    
                        gift 
                    
                        jeans 
                    
                        jewelry 
                    
                        Mobile phone 
                    
                        nice 
                    
                        shirt 
                    
                        shoes 
                    
                        TCP 
            
    
</value>
      <webElementGuid>8ccf8fad-36ef-4d2a-bc10-c4d36bec0f7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page product-tags-all-page&quot;]</value>
      <webElementGuid>91f2aba3-6915-486d-b509-35b0d56b0ef9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[3]</value>
      <webElementGuid>59d9d2b4-114e-4fc2-b45d-9b8212cba4ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[4]</value>
      <webElementGuid>167e2ceb-65f3-46c5-bead-3717e8458acd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div</value>
      <webElementGuid>0b190d31-2612-4028-88e7-e771dbc4e441</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        All product tags
    
    
            
                    
                        apparel 
                    
                        awesome 
                    
                        book 
                    
                        camera 
                    
                        cell 
                    
                        compact 
                    
                        computer 
                    
                        cool 
                    
                        cover 
                    
                        digital 
                    
                        game 
                    
                        gift 
                    
                        jeans 
                    
                        jewelry 
                    
                        Mobile phone 
                    
                        nice 
                    
                        shirt 
                    
                        shoes 
                    
                        TCP 
            
    
' or . = '
    
        All product tags
    
    
            
                    
                        apparel 
                    
                        awesome 
                    
                        book 
                    
                        camera 
                    
                        cell 
                    
                        compact 
                    
                        computer 
                    
                        cool 
                    
                        cover 
                    
                        digital 
                    
                        game 
                    
                        gift 
                    
                        jeans 
                    
                        jewelry 
                    
                        Mobile phone 
                    
                        nice 
                    
                        shirt 
                    
                        shoes 
                    
                        TCP 
            
    
')]</value>
      <webElementGuid>a6ebe29c-6e6b-4f1e-b3db-0d74c77a787b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
