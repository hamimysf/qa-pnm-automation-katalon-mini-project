<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Blue_Sneaker</name>
   <tag></tag>
   <elementGuidId>fcfbf5aa-ad5c-44ee-af54-c3db01477196</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>562487e0-642f-40a9-8aa1-21e4cc191371</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>b3628f92-31da-417a-80ba-d80bc619c5b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>28</value>
      <webElementGuid>b74f2894-ba6f-4c32-b6f1-5e02966854af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            Blue and green Sneaker
        
            
                
                    
                    
                
            
        
            This sleek shoe has all you need for a stylish outfit
        
        
            
                11.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>f5ead662-cba1-400a-ae3e-6b0cfbae71b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>43098018-3f71-4d07-8183-8cf02b4d126a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      <webElementGuid>6355e6ee-75bc-4bfb-99b1-1360b0f0fbda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[4]</value>
      <webElementGuid>12c2c529-34fd-4585-a3f8-a85a9536236a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div</value>
      <webElementGuid>80f73c21-a431-49a7-af5f-1152cebce99d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            Blue and green Sneaker
        
            
                
                    
                    
                
            
        
            This sleek shoe has all you need for a stylish outfit
        
        
            
                11.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            Blue and green Sneaker
        
            
                
                    
                    
                
            
        
            This sleek shoe has all you need for a stylish outfit
        
        
            
                11.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>f3e024c4-e609-4b71-a8b2-547adc0f4391</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
