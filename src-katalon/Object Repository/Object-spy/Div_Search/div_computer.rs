<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_computer</name>
   <tag></tag>
   <elementGuidId>c90632e0-4bf9-46ae-80ca-82217361d89f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.item-box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c06ed7b2-5569-4f44-9aec-2fff366728ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item-box</value>
      <webElementGuid>4761fd2d-1673-44a5-8ee2-26308a56250a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            </value>
      <webElementGuid>22a346ad-9eca-4b71-8304-e48954466422</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]</value>
      <webElementGuid>61b62f74-dcd8-472c-b9eb-2ae68bc8f391</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>858d8d0c-1d73-4318-9dd7-c2ad2fca44a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>9d8173d2-4127-4531-8e69-2819892341fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>2eea6560-6e9d-416d-8ca0-30db3b0fbc21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            ' or . = '
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            ')]</value>
      <webElementGuid>863466be-3c4a-4dc7-84ed-d5b80549ecd5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
