<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Smartphone</name>
   <tag></tag>
   <elementGuidId>dabf2e42-40b5-406b-a584-75ebb11a464a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>07ef06c5-5a01-460a-8a74-8b99b345dbae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>d273b4e4-02ab-4ba3-9427-10110d0e791b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>43</value>
      <webElementGuid>9ee109e5-00c3-4304-b553-2a360f7d36f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            Smartphone
        
            
                
                    
                    
                
            
        
            Newest Tricentis smartphone
        
        
            
                100.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>c5f72971-9b20-4836-95ac-eec9939cbbd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page product-tag-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>0d75dd38-7c85-4f10-bedd-9c8eff5614c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>89531659-588f-4160-b3f6-e60c62099472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>23bea3c8-2c52-43d1-9f9d-0282f2a2156d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div</value>
      <webElementGuid>aacc1fd6-0707-4cd8-92c0-eb8f9acbf1ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            Smartphone
        
            
                
                    
                    
                
            
        
            Newest Tricentis smartphone
        
        
            
                100.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            Smartphone
        
            
                
                    
                    
                
            
        
            Newest Tricentis smartphone
        
        
            
                100.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>f576be70-9b7f-4ba9-b423-da0d6bcd0562</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
