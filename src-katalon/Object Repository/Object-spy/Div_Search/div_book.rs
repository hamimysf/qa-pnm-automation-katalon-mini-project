<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_book</name>
   <tag></tag>
   <elementGuidId>65dc2707-384e-437d-9af4-af4e75d1b9e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.item-box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>51986776-0ca3-4820-9d7f-c81c96518a01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item-box</value>
      <webElementGuid>a366b44d-5a6b-4497-8aa4-e36e0c762b37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                

    
        
            
        
    
    
        
            Health Book
        
            
                
                    
                    
                
            
        
            Worried about your health. Get the newest insights here!
        
        
            
                    27.00
                10.00
            
            
                
                    
            
            
        
    


                            </value>
      <webElementGuid>571a9d50-0e3c-4a01-b9d0-ca82829bb904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]</value>
      <webElementGuid>2dff9c0e-4321-4a21-ab6d-1aeabdf70ee9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>05205a6e-1706-4ffe-8cb8-6291639edab1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>5d5f707a-f095-4fb5-9f0d-f64650faa18d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>d6947f07-6666-41a5-b863-44f011fb7cfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                

    
        
            
        
    
    
        
            Health Book
        
            
                
                    
                    
                
            
        
            Worried about your health. Get the newest insights here!
        
        
            
                    27.00
                10.00
            
            
                
                    
            
            
        
    


                            ' or . = '
                                

    
        
            
        
    
    
        
            Health Book
        
            
                
                    
                    
                
            
        
            Worried about your health. Get the newest insights here!
        
        
            
                    27.00
                10.00
            
            
                
                    
            
            
        
    


                            ')]</value>
      <webElementGuid>6725f22a-a6ac-404e-ae25-33675fe729e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
