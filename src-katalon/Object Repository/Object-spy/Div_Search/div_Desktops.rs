<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Desktops</name>
   <tag></tag>
   <elementGuidId>dc6fd978-ccd1-4d6d-8546-a2e458d2e9a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Computers'])[5]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.item-box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f673efd0-3f88-43af-862d-15b92b053636</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item-box</value>
      <webElementGuid>a4e5409b-b2e4-49f2-bdc5-b38cd482aa1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            
                                
                                    Desktops
                            
                            
                                
                                    
                            
                        
                    </value>
      <webElementGuid>6583d8e6-fc28-4b15-af80-d734374ba246</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;sub-category-grid&quot;]/div[@class=&quot;item-box&quot;]</value>
      <webElementGuid>7106748a-61e7-46a5-8138-190415289921</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Computers'])[5]/following::div[3]</value>
      <webElementGuid>e8ab5e05-fae7-47e1-b63b-1ede6d032283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Computers'])[4]/following::div[5]</value>
      <webElementGuid>9929c764-1c79-40a2-8a50-f3fc9d59a3d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notebooks'])[4]/preceding::div[3]</value>
      <webElementGuid>06da476e-ca84-445e-abb5-b646ab3da3c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div/div</value>
      <webElementGuid>c0d9aaaa-ac77-4fd4-8868-9c2908242b96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            
                                
                                    Desktops
                            
                            
                                
                                    
                            
                        
                    ' or . = '
                        
                            
                                
                                    Desktops
                            
                            
                                
                                    
                            
                        
                    ')]</value>
      <webElementGuid>5f2c9fc2-f11e-4052-b1fb-d6bc2a3950f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
