<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Shirt</name>
   <tag></tag>
   <elementGuidId>36e61a5d-7328-4d98-b70b-837132250ec3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>767e0602-7b49-4a6d-a16a-31c99572a447</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>6a1862ab-fdf6-4ee7-8e93-5463de8a9847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>5</value>
      <webElementGuid>3c30e59e-90a0-4bad-8f9c-8f2c14d687c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            50's Rockabilly Polka Dot Top JR Plus Size
        
            
                
                    
                    
                
            
        
            
        
        
            
                11.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>87604173-dd0a-42b4-ae48-b425d132fcd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page product-tag-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>6f1e10d4-d7db-4156-a469-0617813aafc5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>64a289c5-731f-4620-8072-a21fb505c9fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>e557f334-6665-4ad8-813c-b123f6b7b861</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Custom T-Shirt'])[1]/preceding::div[11]</value>
      <webElementGuid>3e320ef6-e39b-42cc-8a13-5704d18cfaf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div</value>
      <webElementGuid>fd2d9567-fc32-4ade-bf11-634f3a60f927</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
        
            
        
    
    
        
            50&quot; , &quot;'&quot; , &quot;s Rockabilly Polka Dot Top JR Plus Size
        
            
                
                    
                    
                
            
        
            
        
        
            
                11.00
            
            
                
                    
            
            
        
    
&quot;) or . = concat(&quot;
    
        
            
        
    
    
        
            50&quot; , &quot;'&quot; , &quot;s Rockabilly Polka Dot Top JR Plus Size
        
            
                
                    
                    
                
            
        
            
        
        
            
                11.00
            
            
                
                    
            
            
        
    
&quot;))]</value>
      <webElementGuid>be1e37ac-b659-4afd-b702-b0e1463108d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
