<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Camera</name>
   <tag></tag>
   <elementGuidId>3001949d-ec33-4a04-8def-2439f4abb03a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Electronics'])[5]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.item-box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9be2b912-36f9-47d9-bf72-b82be5b0d3a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item-box</value>
      <webElementGuid>ffcfb3c0-62fd-4278-86e1-2591b148a5d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            
                                
                                    Camera, photo
                            
                            
                                
                                    
                            
                        
                    </value>
      <webElementGuid>209b7428-98b2-4ad6-8efc-33f08455a764</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;sub-category-grid&quot;]/div[@class=&quot;item-box&quot;]</value>
      <webElementGuid>e682942b-400c-4370-80b1-5f9d5803f77e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Electronics'])[5]/following::div[3]</value>
      <webElementGuid>63cbe5d2-cdcd-4457-8b94-78eb8355154f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Electronics'])[4]/following::div[5]</value>
      <webElementGuid>699dd98f-7a5b-4fb9-b7ce-e350f8f062c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell phones'])[4]/preceding::div[3]</value>
      <webElementGuid>1d3e2fc9-0cf8-4764-b026-28a8dd623c45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div/div</value>
      <webElementGuid>d0556dd3-eb51-418e-a3c1-f944999ba9e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            
                                
                                    Camera, photo
                            
                            
                                
                                    
                            
                        
                    ' or . = '
                        
                            
                                
                                    Camera, photo
                            
                            
                                
                                    
                            
                        
                    ')]</value>
      <webElementGuid>144adbc4-56d1-4308-b086-8effe8950599</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
