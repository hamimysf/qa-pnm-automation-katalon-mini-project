<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_giftcard</name>
   <tag></tag>
   <elementGuidId>71720cc1-a4bb-403c-86bc-701bf079dba7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>95051063-edb3-4b00-8b0b-62a60e1a2483</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>c2ede559-eb72-4d2e-9f92-e3af5190cbce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>7a36738c-7bad-47a0-8276-4c372a56a3fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            $5 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $5 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                5.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>d21f52a3-ec3b-4e0d-a0ee-afd9a00935d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>d9ef38dd-c4a7-4b82-a9b3-ae9c7f84daed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      <webElementGuid>9b48b59d-2837-42b0-81cf-4658143e1f4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[4]</value>
      <webElementGuid>9ab1e06b-505b-4c95-bfb8-bfefa201cf16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$25 Virtual Gift Card'])[1]/preceding::div[11]</value>
      <webElementGuid>4f5002c4-0dc1-4c54-99bc-4f58855b5544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>ecd5c521-ddb8-4779-9ae8-8aaffa2be938</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            $5 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $5 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                5.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            $5 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $5 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                5.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>e34862d5-ff50-4cd5-ba70-8ce307d2125e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
