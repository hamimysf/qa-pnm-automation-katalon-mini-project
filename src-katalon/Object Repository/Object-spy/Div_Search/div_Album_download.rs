<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Album_download</name>
   <tag></tag>
   <elementGuidId>13344fd7-09da-4432-9958-c08f13dc85f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>609de473-7c5c-411d-aafd-153b4b3069a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>2745dcf0-3f36-4eab-a907-e31e0008124b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>53</value>
      <webElementGuid>47652e4e-62b7-4733-bee2-eb1075151a9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            3rd Album
        
            
                
                    
                    
                
            
        
            3rd album of the famous Tricentis band
        
        
            
                1.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>91dfe0d6-125a-4a2a-9017-6e5bf1a1efe9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>2a8994e8-4307-49f9-a35c-9bf87b9ce5c1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      <webElementGuid>e2d136ed-1b38-434c-9cc2-06c37603e138</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[4]</value>
      <webElementGuid>5c7f85c1-4d3f-4ab4-a683-826e8827810a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Music 2'])[1]/preceding::div[11]</value>
      <webElementGuid>57b756de-2e97-4236-a484-1aa117cb6820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Music 2'])[2]/preceding::div[22]</value>
      <webElementGuid>ca16c243-8534-43e4-a526-ea996d0811b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>e70ccd96-bd64-4d51-9691-d86fb4c8ba96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            3rd Album
        
            
                
                    
                    
                
            
        
            3rd album of the famous Tricentis band
        
        
            
                1.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            3rd Album
        
            
                
                    
                    
                
            
        
            3rd album of the famous Tricentis band
        
        
            
                1.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>9f1617f6-de6f-408d-a65c-953e4723a538</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
