<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Handycam</name>
   <tag></tag>
   <elementGuidId>786cf271-b107-42b4-9bc9-760be524b3ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>91715f24-337f-45e5-a065-328fb962f7b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>55e3e319-b6a5-4eb1-94ed-0cd78d413037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>44</value>
      <webElementGuid>0d289dc2-7cfa-49b1-8a38-7090ae403e51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            1MP 60GB Hard Drive Handycam Camcorder
        
            
                
                    
                    
                
            
        
            Capture video to hard disk drive; 60 GB storage
        
        
            
                349.00
            
            
                
            
            
        
    
</value>
      <webElementGuid>4a060d5e-62b8-4bb6-81dc-e24d811f6235</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page product-tag-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>b3b6ea0f-8ef5-4baa-8a0e-c452e1d36101</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>c22f04e9-7924-4afd-bd35-4541ce4c76a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>f80c8d74-5b98-40db-bc1d-10fda783daf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Camcorder'])[1]/preceding::div[11]</value>
      <webElementGuid>e9cca037-15c6-4f6e-b3aa-72a3c4cb29b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div</value>
      <webElementGuid>76717587-2aa3-4dcf-91f2-f796f57de4c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            1MP 60GB Hard Drive Handycam Camcorder
        
            
                
                    
                    
                
            
        
            Capture video to hard disk drive; 60 GB storage
        
        
            
                349.00
            
            
                
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            1MP 60GB Hard Drive Handycam Camcorder
        
            
                
                    
                    
                
            
        
            Capture video to hard disk drive; 60 GB storage
        
        
            
                349.00
            
            
                
            
            
        
    
')]</value>
      <webElementGuid>d4a9a13d-7872-439d-a1bb-bf5897b730d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
