<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_jewelry</name>
   <tag></tag>
   <elementGuidId>0cec2ef7-e92b-433f-9aa6-975bbfc78705</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div[3]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>56bcce96-939d-4387-a65b-029160b762dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>c273166a-d3af-4654-bb0f-79eff36fa5b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>71</value>
      <webElementGuid>5377b9a9-0b8d-4da0-b166-2e4db56c1e0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>82390a3c-f2ab-4461-a92b-0bec99181b9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page category-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>0826325d-b13d-44c5-bb4f-40f764330c2b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>32db5c64-ce96-49d8-904c-2cdc89dffa3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>7af963d0-77d0-48f8-9174-a38389a76937</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
