<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Laptop</name>
   <tag></tag>
   <elementGuidId>e56cfecc-59fb-43f1-b702-c32b716b6cd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6c648aef-b7b1-4850-bcbe-b649092160b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>62cd0cd2-5f71-4e91-93aa-ad0054457e99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>31</value>
      <webElementGuid>8edbbe66-88b7-403d-93c1-231f45c7ceb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>a1ecb4fc-9e23-4817-9b76-275b3fd9b473</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page product-tag-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>fbfc1907-2b94-4eb2-afe0-fdec14bfc06a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>db7457f3-3870-48ea-8fcd-c039cad8ba72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>a7fefb71-fd8c-4049-8a8f-43e0c673a51c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Smartphone'])[1]/preceding::div[11]</value>
      <webElementGuid>57c42ef0-42e7-4579-9350-a57bb62162c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div</value>
      <webElementGuid>d11a4197-29d6-4171-8923-78c2d1d37abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>59ff8c33-8327-4d72-8a20-4a429925615c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
