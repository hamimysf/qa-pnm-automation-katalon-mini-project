<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Right Menu</name>
   <tag></tag>
   <elementGuidId>436fcf55-fe82-4789-8413-105d8a65ce61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='View all'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.rightside-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>26322d1c-49e9-4926-9ac3-ddb4aba62a6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rightside-3</value>
      <webElementGuid>ade99167-0c0e-4c04-89a5-7e7477c7afb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

    
        Newsletter
    
    
        
            Sign up for our newsletter:
            
            
            
            
            
                
                Wait...
            

        
        
    
    
        $(document).ready(function () {
            $('#newsletter-subscribe-button').click(function () {
                
                var email = $(&quot;#newsletter-email&quot;).val();
                var subscribeProgress = $(&quot;#subscribe-loading-progress&quot;);
                subscribeProgress.show();
                $.ajax({
                    cache: false,
                    type: &quot;POST&quot;,
                    url: &quot;/subscribenewsletter&quot;,
                    data: { &quot;email&quot;: email },
                    success: function (data) {
                        subscribeProgress.hide();
                        $(&quot;#newsletter-result-block&quot;).html(data.Result);
                         if (data.Success) {
                            $('#newsletter-subscribe-block').hide();
                            $('#newsletter-result-block').show();
                         }
                         else {
                            $('#newsletter-result-block').fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                         }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                        alert('Failed to subscribe.');
                        subscribeProgress.hide();
                    }  
                });                
                return false;
            });
        });
    


    
        Community poll
    
    
        
    Do you like nopCommerce?
        
                
                    
                    Excellent
                
                
                    
                    Good
                
                
                    
                    Poor
                
                
                    
                    Very bad
                
        
        
            
            Wait...
        
        
        
        
            $(document).ready(function () {
                $('#vote-poll-1').click(function () {
                var pollAnswerId = $(&quot;input:radio[name=pollanswers-1]:checked&quot;).val();
                if (typeof (pollAnswerId) == 'undefined') {
                    alert('Please select an answer');
                }
                else {
                    var voteProgress = $(&quot;#poll-voting-progress-1&quot;);
                    voteProgress.show();
                    $.ajax({
                        cache: false,
                        type: &quot;POST&quot;,
                        url: &quot;/poll/vote&quot;,
                        data: { &quot;pollAnswerId&quot;: pollAnswerId },
                        success: function (data) {
                            voteProgress.hide();

                            if (data.error) {
                                $(&quot;#block-poll-vote-error-1&quot;).html(data.error);
                                $('#block-poll-vote-error-1').fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                            }

                            if (data.html) {
                                $(&quot;#poll-block-1&quot;).replaceWith(data.html);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('Failed to vote.');
                            voteProgress.hide();
                        }
                    });
                }
                return false;
            });
        });
        


    

</value>
      <webElementGuid>741d0ed1-797d-4ec7-a627-17363f22448e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;rightside-3&quot;]</value>
      <webElementGuid>683f417c-832d-43d7-92cf-9cc09ca94c07</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View all'])[1]/following::div[1]</value>
      <webElementGuid>1fd3f948-c725-4ebf-b9aa-62b3d196a665</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TCP'])[1]/following::div[2]</value>
      <webElementGuid>3b118b30-b0a3-4835-8405-5a14cec72412</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>c2d95106-b0fe-49ed-89f1-1df194882504</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;

    
        Newsletter
    
    
        
            Sign up for our newsletter:
            
            
            
            
            
                
                Wait...
            

        
        
    
    
        $(document).ready(function () {
            $(&quot; , &quot;'&quot; , &quot;#newsletter-subscribe-button&quot; , &quot;'&quot; , &quot;).click(function () {
                
                var email = $(&quot;#newsletter-email&quot;).val();
                var subscribeProgress = $(&quot;#subscribe-loading-progress&quot;);
                subscribeProgress.show();
                $.ajax({
                    cache: false,
                    type: &quot;POST&quot;,
                    url: &quot;/subscribenewsletter&quot;,
                    data: { &quot;email&quot;: email },
                    success: function (data) {
                        subscribeProgress.hide();
                        $(&quot;#newsletter-result-block&quot;).html(data.Result);
                         if (data.Success) {
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-subscribe-block&quot; , &quot;'&quot; , &quot;).hide();
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-result-block&quot; , &quot;'&quot; , &quot;).show();
                         }
                         else {
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-result-block&quot; , &quot;'&quot; , &quot;).fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                         }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                        alert(&quot; , &quot;'&quot; , &quot;Failed to subscribe.&quot; , &quot;'&quot; , &quot;);
                        subscribeProgress.hide();
                    }  
                });                
                return false;
            });
        });
    


    
        Community poll
    
    
        
    Do you like nopCommerce?
        
                
                    
                    Excellent
                
                
                    
                    Good
                
                
                    
                    Poor
                
                
                    
                    Very bad
                
        
        
            
            Wait...
        
        
        
        
            $(document).ready(function () {
                $(&quot; , &quot;'&quot; , &quot;#vote-poll-1&quot; , &quot;'&quot; , &quot;).click(function () {
                var pollAnswerId = $(&quot;input:radio[name=pollanswers-1]:checked&quot;).val();
                if (typeof (pollAnswerId) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot;) {
                    alert(&quot; , &quot;'&quot; , &quot;Please select an answer&quot; , &quot;'&quot; , &quot;);
                }
                else {
                    var voteProgress = $(&quot;#poll-voting-progress-1&quot;);
                    voteProgress.show();
                    $.ajax({
                        cache: false,
                        type: &quot;POST&quot;,
                        url: &quot;/poll/vote&quot;,
                        data: { &quot;pollAnswerId&quot;: pollAnswerId },
                        success: function (data) {
                            voteProgress.hide();

                            if (data.error) {
                                $(&quot;#block-poll-vote-error-1&quot;).html(data.error);
                                $(&quot; , &quot;'&quot; , &quot;#block-poll-vote-error-1&quot; , &quot;'&quot; , &quot;).fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                            }

                            if (data.html) {
                                $(&quot;#poll-block-1&quot;).replaceWith(data.html);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(&quot; , &quot;'&quot; , &quot;Failed to vote.&quot; , &quot;'&quot; , &quot;);
                            voteProgress.hide();
                        }
                    });
                }
                return false;
            });
        });
        


    

&quot;) or . = concat(&quot;

    
        Newsletter
    
    
        
            Sign up for our newsletter:
            
            
            
            
            
                
                Wait...
            

        
        
    
    
        $(document).ready(function () {
            $(&quot; , &quot;'&quot; , &quot;#newsletter-subscribe-button&quot; , &quot;'&quot; , &quot;).click(function () {
                
                var email = $(&quot;#newsletter-email&quot;).val();
                var subscribeProgress = $(&quot;#subscribe-loading-progress&quot;);
                subscribeProgress.show();
                $.ajax({
                    cache: false,
                    type: &quot;POST&quot;,
                    url: &quot;/subscribenewsletter&quot;,
                    data: { &quot;email&quot;: email },
                    success: function (data) {
                        subscribeProgress.hide();
                        $(&quot;#newsletter-result-block&quot;).html(data.Result);
                         if (data.Success) {
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-subscribe-block&quot; , &quot;'&quot; , &quot;).hide();
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-result-block&quot; , &quot;'&quot; , &quot;).show();
                         }
                         else {
                            $(&quot; , &quot;'&quot; , &quot;#newsletter-result-block&quot; , &quot;'&quot; , &quot;).fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                         }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                        alert(&quot; , &quot;'&quot; , &quot;Failed to subscribe.&quot; , &quot;'&quot; , &quot;);
                        subscribeProgress.hide();
                    }  
                });                
                return false;
            });
        });
    


    
        Community poll
    
    
        
    Do you like nopCommerce?
        
                
                    
                    Excellent
                
                
                    
                    Good
                
                
                    
                    Poor
                
                
                    
                    Very bad
                
        
        
            
            Wait...
        
        
        
        
            $(document).ready(function () {
                $(&quot; , &quot;'&quot; , &quot;#vote-poll-1&quot; , &quot;'&quot; , &quot;).click(function () {
                var pollAnswerId = $(&quot;input:radio[name=pollanswers-1]:checked&quot;).val();
                if (typeof (pollAnswerId) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot;) {
                    alert(&quot; , &quot;'&quot; , &quot;Please select an answer&quot; , &quot;'&quot; , &quot;);
                }
                else {
                    var voteProgress = $(&quot;#poll-voting-progress-1&quot;);
                    voteProgress.show();
                    $.ajax({
                        cache: false,
                        type: &quot;POST&quot;,
                        url: &quot;/poll/vote&quot;,
                        data: { &quot;pollAnswerId&quot;: pollAnswerId },
                        success: function (data) {
                            voteProgress.hide();

                            if (data.error) {
                                $(&quot;#block-poll-vote-error-1&quot;).html(data.error);
                                $(&quot; , &quot;'&quot; , &quot;#block-poll-vote-error-1&quot; , &quot;'&quot; , &quot;).fadeIn(&quot;slow&quot;).delay(2000).fadeOut(&quot;slow&quot;);
                            }

                            if (data.html) {
                                $(&quot;#poll-block-1&quot;).replaceWith(data.html);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(&quot; , &quot;'&quot; , &quot;Failed to vote.&quot; , &quot;'&quot; , &quot;);
                            voteProgress.hide();
                        }
                    });
                }
                return false;
            });
        });
        


    

&quot;))]</value>
      <webElementGuid>8545775a-ca2b-471d-bf75-cfe60683b2bc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
