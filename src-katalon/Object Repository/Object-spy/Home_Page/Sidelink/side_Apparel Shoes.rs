<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>side_Apparel Shoes</name>
   <tag></tag>
   <elementGuidId>8b07a5c2-737e-4c84-938e-746979638dbc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Apparel &amp; Shoes')])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8d6ed69f-7b08-4f09-9876-77b4fa47eb72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/apparel-shoes</value>
      <webElementGuid>58798cc8-6ca5-4efb-b9ac-6bc625379024</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apparel &amp; Shoes
        </value>
      <webElementGuid>6a7b21cc-2109-4785-8dad-f5b8f810a65f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;side-2&quot;]/div[@class=&quot;block block-category-navigation&quot;]/div[@class=&quot;listbox&quot;]/ul[@class=&quot;list&quot;]/li[@class=&quot;inactive&quot;]/a[1]</value>
      <webElementGuid>662c915f-bd2c-47c7-a4fc-62d8348eb98c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Apparel &amp; Shoes')])[3]</value>
      <webElementGuid>80d4ac1f-d5f5-4bb0-8102-1771904bf667</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Electronics'])[3]/following::a[1]</value>
      <webElementGuid>098de992-06f6-4e30-8554-124a92982f49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Computers'])[3]/following::a[2]</value>
      <webElementGuid>fb57ff8e-ebf8-4a73-bb23-975a85e2253d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Digital downloads'])[3]/preceding::a[1]</value>
      <webElementGuid>874f83cb-f45a-43e2-beb0-e615526fdc57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[3]/preceding::a[2]</value>
      <webElementGuid>25357014-2d37-4279-a98c-b84a99fbed70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/apparel-shoes')])[3]</value>
      <webElementGuid>53007f4d-add0-4bfe-ad6f-af6899dd32b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/ul/li[4]/a</value>
      <webElementGuid>5f44d40c-ac9e-411c-9ea3-c61de4faabe9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/apparel-shoes' and (text() = 'Apparel &amp; Shoes
        ' or . = 'Apparel &amp; Shoes
        ')]</value>
      <webElementGuid>ca23382b-dfc7-4f15-a9f4-529b40c26de9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
