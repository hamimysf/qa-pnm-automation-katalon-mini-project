<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>side_Gift Cards</name>
   <tag></tag>
   <elementGuidId>ccc7f794-8564-42d6-bfe3-02bbc1ada025</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Gift Cards')])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.active > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>99fff811-da4f-4b36-a87f-fe2b8199dc53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/gift-cards</value>
      <webElementGuid>6d5e68c9-dade-40b7-94cb-770be9717e28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gift Cards
        </value>
      <webElementGuid>7281c7c0-9725-4992-9d39-f3dca3d77ecf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;side-2&quot;]/div[@class=&quot;block block-category-navigation&quot;]/div[@class=&quot;listbox&quot;]/ul[@class=&quot;list&quot;]/li[@class=&quot;active&quot;]/a[1]</value>
      <webElementGuid>271f6b5c-a1ab-4af4-9d4e-001a3f71982c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Gift Cards')])[3]</value>
      <webElementGuid>cbafa423-ad6e-4e1d-803c-1d4ff7579195</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[3]/following::a[1]</value>
      <webElementGuid>3f04742d-4c6c-4943-a679-976f0a077a0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Digital downloads'])[3]/following::a[2]</value>
      <webElementGuid>99428143-bcb6-4425-862f-5c23a8d30213</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manufacturers'])[1]/preceding::a[1]</value>
      <webElementGuid>88eaa6e8-45fa-4736-9c8a-66594df2bf9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tricentis'])[1]/preceding::a[1]</value>
      <webElementGuid>fe4a8df4-abe3-4809-b7d3-6302ddd7f355</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/gift-cards')])[3]</value>
      <webElementGuid>b965cd8c-d62d-4781-a5f0-089751760b00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/ul/li[7]/a</value>
      <webElementGuid>d7cacb68-a9ed-4656-8e9e-3f1d1d2952ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/gift-cards' and (text() = 'Gift Cards
        ' or . = 'Gift Cards
        ')]</value>
      <webElementGuid>ddaffedc-bd75-4186-9fcb-1bbd8bf0a2f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
