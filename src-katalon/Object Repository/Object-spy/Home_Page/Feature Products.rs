<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Feature Products</name>
   <tag></tag>
   <elementGuidId>4cd6aae1-6e56-4327-9f3d-7eadb8129991</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to our store'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-grid.home-page-product-grid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8e1611ff-ecaf-4247-bf8a-d2141a94e869</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-grid home-page-product-grid</value>
      <webElementGuid>13001452-d0ba-457b-ab1e-0bbf039331fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            Featured products
        
            
                

    
        
            
        
    
    
        
            $25 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $25 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                25.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1200.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
    </value>
      <webElementGuid>a8a402b2-334d-4f7d-b85d-dbd3fb88a72f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page home-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid home-page-product-grid&quot;]</value>
      <webElementGuid>2c21379f-930f-4906-9407-ef1091df1b4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to our store'])[1]/following::div[2]</value>
      <webElementGuid>f2d9d3a7-f426-40df-9539-0a1dd914f610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::div[37]</value>
      <webElementGuid>2e6da61a-31f5-4dd8-8c56-3dcc113b8dee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[3]</value>
      <webElementGuid>d458e6bd-201f-4dba-8286-2fba5baf068e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            Featured products
        
            
                

    
        
            
        
    
    
        
            $25 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $25 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                25.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1200.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
    ' or . = '
        
            Featured products
        
            
                

    
        
            
        
    
    
        
            $25 Virtual Gift Card
        
            
                
                    
                    
                
            
        
            $25 Gift Card. Gift Cards must be redeemed through our site Web site toward the purchase of eligible products.
        
        
            
                25.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            14.1-inch Laptop
        
            
                
                    
                    
                
            
        
            Unique Asian-influenced imprint wraps the laptop both inside and out
        
        
            
                1590.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1200.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


            
            
                

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


            
    ')]</value>
      <webElementGuid>c5109415-7087-46a5-86af-6f6e1e1f581d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
