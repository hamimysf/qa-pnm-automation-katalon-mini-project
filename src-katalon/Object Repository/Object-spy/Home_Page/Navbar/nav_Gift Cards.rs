<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_Gift Cards</name>
   <tag></tag>
   <elementGuidId>4768dbe9-67fb-4cdd-90f4-b613229140e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Gift Cards')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>aeecd4af-5416-455e-929f-c0f1c7904442</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/gift-cards</value>
      <webElementGuid>1edc3597-bdc2-44e9-9ecf-353c7b0591b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hover</value>
      <webElementGuid>68c965e0-5899-4f68-878b-6ebc14f67c5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gift Cards
        </value>
      <webElementGuid>de102814-6b91-45db-bff7-206a7cd1dbdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header-menu&quot;]/ul[@class=&quot;top-menu&quot;]/li[7]/a[@class=&quot;hover&quot;]</value>
      <webElementGuid>9a41b1fa-0442-44a7-ad5e-817efbe042ca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Gift Cards')]</value>
      <webElementGuid>b8b5ff6e-ba45-4726-8d9d-825c9dbace3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[1]/following::a[1]</value>
      <webElementGuid>18c412a8-64ce-4242-acd0-c0aa6bc6d902</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Digital downloads'])[1]/following::a[2]</value>
      <webElementGuid>e6e163c5-01bb-489e-b556-069482126906</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[1]/preceding::a[1]</value>
      <webElementGuid>2326a070-e2dd-40f5-b0f9-0adedb179db4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Books'])[2]/preceding::a[2]</value>
      <webElementGuid>9ca9e972-0b5a-40c8-b749-84568ac552d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Gift Cards']/parent::*</value>
      <webElementGuid>1c12df7a-776c-4fed-9eb6-6dfe2cc0b1d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/gift-cards')]</value>
      <webElementGuid>ecda5d08-84f8-4324-b196-fe4f9fa97f35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/a</value>
      <webElementGuid>5788874e-ca94-487c-8511-0feea7943071</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/gift-cards' and (text() = 'Gift Cards
        ' or . = 'Gift Cards
        ')]</value>
      <webElementGuid>0023877c-3c5c-4d38-9e90-2ffe38bff614</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
