<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_Apparel  Shoes</name>
   <tag></tag>
   <elementGuidId>3c0b4192-7847-4909-b0f9-0ef556589b20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Apparel &amp; Shoes')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>04c6785c-771d-4896-a0f8-71d6516f071e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/apparel-shoes</value>
      <webElementGuid>add304a8-8b3f-4006-9fc7-0a5c7d29f9c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hover</value>
      <webElementGuid>2ae012f6-1275-49d7-8199-4b5bdd748acf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apparel &amp; Shoes
        </value>
      <webElementGuid>3b3e5011-2a4a-4072-9b69-2f5c895c8017</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header-menu&quot;]/ul[@class=&quot;top-menu&quot;]/li[4]/a[@class=&quot;hover&quot;]</value>
      <webElementGuid>9da05887-d693-4c7c-b42a-52d7285d1a4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Apparel &amp; Shoes')]</value>
      <webElementGuid>67399218-594d-4739-9cf9-da86af02724e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell phones'])[1]/following::a[1]</value>
      <webElementGuid>37d14c16-82f7-411a-924c-d14641176084</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Camera, photo'])[1]/following::a[2]</value>
      <webElementGuid>30c3bf25-827d-446d-a0d2-c05cf3eb8cfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Digital downloads'])[1]/preceding::a[1]</value>
      <webElementGuid>7ec8a165-e436-4ddd-b774-7429d7e4d2b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[1]/preceding::a[2]</value>
      <webElementGuid>abd579ea-0464-454d-9933-a7860eeceabb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apparel &amp; Shoes']/parent::*</value>
      <webElementGuid>58f614cb-2743-4cb2-b45b-6ed2e8629379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/apparel-shoes')]</value>
      <webElementGuid>bad7592b-e637-4019-a554-f05c309c0c5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[4]/a</value>
      <webElementGuid>09944247-8e89-4880-8efd-585499515a17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/apparel-shoes' and (text() = 'Apparel &amp; Shoes
        ' or . = 'Apparel &amp; Shoes
        ')]</value>
      <webElementGuid>d6733ea9-869b-417e-85e3-0676aeb4e6ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
