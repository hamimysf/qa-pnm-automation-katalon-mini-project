<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_Digital downloads</name>
   <tag></tag>
   <elementGuidId>7364eca3-9d02-4fa7-aa09-fb6b415cb86b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Digital downloads')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0b22e5bd-fc3a-4b3c-beb4-640a038133b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/digital-downloads</value>
      <webElementGuid>e021bca8-23ea-40ca-ace7-ae60858644a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hover</value>
      <webElementGuid>2ece8297-d81d-4f8e-b92f-39c9a9ed07b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Digital downloads
        </value>
      <webElementGuid>70863966-af24-4432-9910-736293a57230</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header-menu&quot;]/ul[@class=&quot;top-menu&quot;]/li[5]/a[@class=&quot;hover&quot;]</value>
      <webElementGuid>2c048f23-6f29-4a53-aaab-e3452c6c15eb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Digital downloads')]</value>
      <webElementGuid>26d8cbeb-79b2-4097-8be2-5f27d421d99c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apparel &amp; Shoes'])[1]/following::a[1]</value>
      <webElementGuid>04bb7e8e-7abc-40b4-bc7e-07638e17deb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell phones'])[1]/following::a[2]</value>
      <webElementGuid>e0ce23d4-f3e4-4eb5-a771-8726ef1275b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[1]/preceding::a[1]</value>
      <webElementGuid>6067e571-b833-4052-beee-34352846e4a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[1]/preceding::a[2]</value>
      <webElementGuid>f31804c7-193c-4777-83d9-fa6bf520a442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Digital downloads']/parent::*</value>
      <webElementGuid>c88dba99-97ca-4158-8443-01a34248ff6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/digital-downloads')]</value>
      <webElementGuid>20bcb942-4ab0-43b9-97dc-9bb45e3298f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a</value>
      <webElementGuid>359317e3-635d-4719-b797-9dfd94be3e48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/digital-downloads' and (text() = 'Digital downloads
        ' or . = 'Digital downloads
        ')]</value>
      <webElementGuid>b087ab4d-41fe-4788-b78f-afa6e1bd930e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
