<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_Electronics</name>
   <tag></tag>
   <elementGuidId>43a9ddfb-fb35-4f36-b597-079b8cc250db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Electronics')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>45fe7b42-4335-41c7-8e0c-ea11bfc77af1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/electronics</value>
      <webElementGuid>b122f943-2e8d-41a4-8a32-d131b52814c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hover</value>
      <webElementGuid>8d45197c-c384-4a4c-a113-e837161c6b92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Electronics
        </value>
      <webElementGuid>8ed4da32-8b25-4397-aaa0-70f12bd70e8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header-menu&quot;]/ul[@class=&quot;top-menu&quot;]/li[3]/a[@class=&quot;hover&quot;]</value>
      <webElementGuid>e60f03f3-8e71-493e-b5d3-dfff310bc7f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Electronics')]</value>
      <webElementGuid>21be975e-744e-43ac-8794-029e093ba027</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Accessories'])[1]/following::a[1]</value>
      <webElementGuid>7e8b34c0-dba4-4387-8a77-f130f092b10b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notebooks'])[1]/following::a[2]</value>
      <webElementGuid>ee4dda6c-a5ee-45d4-b0c0-72658213f6a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Camera, photo'])[1]/preceding::a[1]</value>
      <webElementGuid>2866a2bf-fc20-4345-96d9-0ebc135228aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell phones'])[1]/preceding::a[2]</value>
      <webElementGuid>a652ac97-e020-457a-8f40-54862f8dfa47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Electronics']/parent::*</value>
      <webElementGuid>690797a9-b53b-4865-a44f-e27e48d26d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/electronics')]</value>
      <webElementGuid>7b2bb52c-49b4-408d-a874-1310ee6c8d17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[3]/a</value>
      <webElementGuid>b8d59fc7-80e9-45b0-9707-22cb13383952</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/electronics' and (text() = 'Electronics
        ' or . = 'Electronics
        ')]</value>
      <webElementGuid>e129b89f-653c-47a5-a24d-1c4371d66bbf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
