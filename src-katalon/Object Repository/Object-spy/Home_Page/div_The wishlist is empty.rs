<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_The wishlist is empty</name>
   <tag></tag>
   <elementGuidId>27fe6dc4-e34e-462d-8444-41263c6ca8d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wishlist'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.wishlist-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a119f8ee-0afb-473a-86ff-5268f3bfc343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wishlist-content</value>
      <webElementGuid>d077070d-cd08-4942-b91c-4c522755a401</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
The wishlist is empty!        </value>
      <webElementGuid>b672b097-f764-4608-b80f-fd260c0234b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page wishlist-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;wishlist-content&quot;]</value>
      <webElementGuid>4d0c9974-1626-4257-bd5c-65002f9cea3c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wishlist'])[2]/following::div[2]</value>
      <webElementGuid>13c5b845-42d1-4abf-8613-b121d2bac65e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[9]</value>
      <webElementGuid>e7265295-ee6c-48de-9735-55632cbe15e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[1]</value>
      <webElementGuid>e01bea3f-5eb6-4023-9e1a-ded08075800b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::div[1]</value>
      <webElementGuid>0439baf6-efa8-430e-b907-04259a01fd88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The wishlist is empty!']/parent::*</value>
      <webElementGuid>8e8f39f0-330c-4eb6-a94f-392252204a1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]/div</value>
      <webElementGuid>dbcf389e-6eb2-4732-acf1-ee8f112d715b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
The wishlist is empty!        ' or . = '
The wishlist is empty!        ')]</value>
      <webElementGuid>260139e4-06a5-4a0e-8ae5-2ba4a7af1448</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
