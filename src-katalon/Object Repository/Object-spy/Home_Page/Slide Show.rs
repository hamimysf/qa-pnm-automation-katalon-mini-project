<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Slide Show</name>
   <tag></tag>
   <elementGuidId>aa567d7b-6820-4fa8-83ea-1d67f86256bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.slider-wrapper.theme-default</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2ed77c0f-fbc5-405d-a21b-6d20ff2e9d3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>slider-wrapper theme-default</value>
      <webElementGuid>d4bfbc1c-9c8a-4b31-a990-d4f9312f0b56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
                    
                
            

                    
                
            

        
        
        
    Speed | TricentisPrevNext12
</value>
      <webElementGuid>56651892-0f73-4b14-9efc-6a64c934f881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page home-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;slider-wrapper theme-default&quot;]</value>
      <webElementGuid>b75246d1-a14e-4e44-acec-18a8f0cdcc85</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[5]</value>
      <webElementGuid>d9d63891-41bd-4541-89cd-614a60a9bbb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[6]</value>
      <webElementGuid>4911a292-e9c7-47e6-866b-db8581f60ddf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>4166f33b-70d0-4259-b600-e9383c52f33d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
                    
                
            

                    
                
            

        
        
        
    Speed | TricentisPrevNext12
' or . = '
    
                    
                
            

                    
                
            

        
        
        
    Speed | TricentisPrevNext12
')]</value>
      <webElementGuid>1ef4c95f-215d-4863-a16d-849767e917c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
