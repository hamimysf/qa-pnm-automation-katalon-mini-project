<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Side Menu</name>
   <tag></tag>
   <elementGuidId>1e62e34c-496b-467d-9eec-ac874d6b8242</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.leftside-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>063a3dbe-ff39-4eb8-8bbc-c5072f6c1558</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leftside-3</value>
      <webElementGuid>0ed66109-22cb-4a4d-89c4-8845cb395e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

    
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    
    
        
            Manufacturers
        
        
            
                    Tricentis
                    
            
        
    
    
        
            Popular tags
        
        
            
                
                        apparel
                        awesome
                        book
                        camera
                        cell
                        compact
                        computer
                        cool
                        digital
                        gift
                        jewelry
                        nice
                        shirt
                        shoes
                        TCP
                
            
                
                    View all
                
        
    
</value>
      <webElementGuid>8ef59588-7834-47d2-bddf-93cdc2a85f0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;leftside-3&quot;]</value>
      <webElementGuid>d1a324a4-7c0a-4707-81ef-a6b1e4c1abca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[5]</value>
      <webElementGuid>9e198e95-a360-4ed6-b582-4eda474a95a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[2]/following::div[6]</value>
      <webElementGuid>c8b73fb8-14e1-4af5-8e20-a2ec33f3607a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div</value>
      <webElementGuid>73d4b60c-eed0-4f77-90dc-20098dddfb41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

    
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    
    
        
            Manufacturers
        
        
            
                    Tricentis
                    
            
        
    
    
        
            Popular tags
        
        
            
                
                        apparel
                        awesome
                        book
                        camera
                        cell
                        compact
                        computer
                        cool
                        digital
                        gift
                        jewelry
                        nice
                        shirt
                        shoes
                        TCP
                
            
                
                    View all
                
        
    
' or . = '

    
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    
    
        
            Manufacturers
        
        
            
                    Tricentis
                    
            
        
    
    
        
            Popular tags
        
        
            
                
                        apparel
                        awesome
                        book
                        camera
                        cell
                        compact
                        computer
                        cool
                        digital
                        gift
                        jewelry
                        nice
                        shirt
                        shoes
                        TCP
                
            
                
                    View all
                
        
    
')]</value>
      <webElementGuid>75f3f991-ecdf-48e8-a924-76d61c7bbfd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
