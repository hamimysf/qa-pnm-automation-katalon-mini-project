<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tag_View all</name>
   <tag></tag>
   <elementGuidId>9751fc71-bb67-4869-b88e-6034595f26bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'View all')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.view-all > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>aad7652b-040a-45a6-92e2-f121aad5f39a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/producttag/all</value>
      <webElementGuid>88bd0706-9cc4-48b4-81ca-0ab249d3dd0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>View all</value>
      <webElementGuid>10efd6d4-e786-47cb-bdc0-fba3a522ec51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;leftside-3&quot;]/div[@class=&quot;block block-popular-tags&quot;]/div[@class=&quot;listbox&quot;]/div[@class=&quot;view-all&quot;]/a[1]</value>
      <webElementGuid>d5108f05-fb3c-4716-bf74-d23fa6c30071</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'View all')]</value>
      <webElementGuid>fd306c25-72c8-4aa3-994b-a182c929c122</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TCP'])[1]/following::a[1]</value>
      <webElementGuid>43a5b76c-694c-4046-b3c7-38efb48f973a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='shoes'])[1]/following::a[2]</value>
      <webElementGuid>777bf96f-69fa-4be1-be55-89470e8f75ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::a[1]</value>
      <webElementGuid>2b40f69e-1cba-42e0-9120-b6b226753a26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/preceding::a[1]</value>
      <webElementGuid>2647312a-946b-4918-8399-ae7238b5ddd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='View all']/parent::*</value>
      <webElementGuid>b4859633-889f-486c-b889-d168765ddf5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/producttag/all')]</value>
      <webElementGuid>762aebef-a74b-4bb7-9644-0eaf7f05bbe3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a</value>
      <webElementGuid>33d3aa6e-8e76-41f7-85d3-ebda65d0366d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/producttag/all' and (text() = 'View all' or . = 'View all')]</value>
      <webElementGuid>29cbf600-f2c6-4491-bc44-0e405797332d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
