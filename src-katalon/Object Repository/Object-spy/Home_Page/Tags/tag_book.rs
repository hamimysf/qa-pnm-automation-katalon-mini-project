<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tag_book</name>
   <tag></tag>
   <elementGuidId>94e06e94-aad4-4231-8421-2cb2bc2363b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'book')])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9e1360c0-27f3-4a74-bbfe-a71bb9f5b979</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/producttag/10/book</value>
      <webElementGuid>0f82b4f7-011e-49f2-88bc-6ea3b0f7d5c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>book</value>
      <webElementGuid>bfc91621-0121-4596-9e96-98ccdc7141db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;leftside-3&quot;]/div[@class=&quot;block block-popular-tags&quot;]/div[@class=&quot;listbox&quot;]/div[@class=&quot;tags&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>66750edf-c14e-424a-9010-0b81b7a3f7a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'book')])[3]</value>
      <webElementGuid>a59711df-d72f-49f2-96d6-65e8a55a0c83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='awesome'])[1]/following::a[1]</value>
      <webElementGuid>d71d046e-56a2-4c9f-90ca-d9c8dc5e552d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='apparel'])[1]/following::a[2]</value>
      <webElementGuid>435b2373-90c0-4edc-89a5-69102204698b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='camera'])[1]/preceding::a[1]</value>
      <webElementGuid>7a1999b7-2ace-452c-b59a-d919a828ed05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cell'])[1]/preceding::a[2]</value>
      <webElementGuid>8ee94682-aec3-4cbf-a89e-49877dd8e7ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='book']/parent::*</value>
      <webElementGuid>fcce83d0-6c94-4e0a-a9a9-aa0a5688b8cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/producttag/10/book')]</value>
      <webElementGuid>0c42c4c3-a62e-4dd2-ac24-7e69fa52dc9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/ul/li[3]/a</value>
      <webElementGuid>b62b6bf6-5fae-4cf0-961b-f27bd74b2469</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/producttag/10/book' and (text() = 'book' or . = 'book')]</value>
      <webElementGuid>899ae98a-4627-4852-b98e-92ba514230b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
