<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>categories</name>
   <tag></tag>
   <elementGuidId>1ed3b188-f749-4d36-b42b-f0b28475f2c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.block.block-category-navigation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a0a6fb87-3177-4a2f-95c9-5fd2b3275c4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>block block-category-navigation</value>
      <webElementGuid>641e6c91-b47f-4148-9f1c-b93f37748e8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    </value>
      <webElementGuid>929fa4eb-0873-41b6-8357-67019783c8ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;leftside-3&quot;]/div[@class=&quot;block block-category-navigation&quot;]</value>
      <webElementGuid>f2370f11-1b86-476d-b2b3-a1f67c8fbb81</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[6]</value>
      <webElementGuid>65bd70c6-211b-4e56-a75e-50e255b4ce8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[2]/following::div[7]</value>
      <webElementGuid>6c17a045-bafd-4f75-88d0-e89d940bf25e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div</value>
      <webElementGuid>fb93fd6c-9f58-4c1c-a693-3a22b6d94383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    ' or . = '
        
            Categories
        
        
            
    
        Books
        

    
    
        Computers
        

    
    
        Electronics
        

    
    
        Apparel &amp; Shoes
        

    
    
        Digital downloads
        

    
    
        Jewelry
        

    
    
        Gift Cards
        

    
            
        
    ')]</value>
      <webElementGuid>65c3b676-7136-465a-b64d-1b68568897c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
