<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_wishlist_button</name>
   <tag></tag>
   <elementGuidId>c46f5ce1-f3ca-416e-bc5d-60c9649f69c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='(0)'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.ico-wishlist</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bd20f28c-86ec-40a4-8ba4-f563f5d64425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/wishlist</value>
      <webElementGuid>d2555b93-b863-465f-8558-1ffa8cb67f12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ico-wishlist</value>
      <webElementGuid>ae9a16a1-3e3e-4bcc-a036-174c33e11538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Wishlist
                    (0)
                </value>
      <webElementGuid>888853d9-48ea-4328-b98a-ce6071aab7f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header&quot;]/div[@class=&quot;header-links-wrapper&quot;]/div[@class=&quot;header-links&quot;]/ul[1]/li[4]/a[@class=&quot;ico-wishlist&quot;]</value>
      <webElementGuid>5e76be8c-fffc-4f28-9b74-daaf09e2a971</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(0)'])[1]/following::a[1]</value>
      <webElementGuid>a051e96a-dfdf-48f2-9707-98fdd7a61b22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[1]/following::a[1]</value>
      <webElementGuid>c74cea95-29f9-47c8-b834-dc6a2bc630d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/wishlist')]</value>
      <webElementGuid>26ec12b1-8b27-4771-8984-e31a964c3d55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a</value>
      <webElementGuid>71088698-8aec-45fa-8167-ba9caef4a2ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/wishlist' and (text() = '
                    Wishlist
                    (0)
                ' or . = '
                    Wishlist
                    (0)
                ')]</value>
      <webElementGuid>9c50b16e-4567-40ae-b7a9-3550e4e73696</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
