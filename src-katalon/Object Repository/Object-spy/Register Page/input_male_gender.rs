<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_male_gender</name>
   <tag></tag>
   <elementGuidId>8a38349c-6c28-45c7-b9dd-388618d4d4cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='gender-male']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#gender-male</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b63de3a8-391c-4aa6-b8ab-aafef4bb12eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>gender-male</value>
      <webElementGuid>9dfef266-9730-4357-b5c4-e541a7b0b570</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Gender</value>
      <webElementGuid>ce05f048-b584-4ba2-beb3-13d171eb85ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>2a59d58d-28d3-4777-92ca-fc46eb0c038d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>M</value>
      <webElementGuid>f2e95d96-97b6-4b5f-b590-f9f8d4938a63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gender-male&quot;)</value>
      <webElementGuid>b866a8e5-ea9e-40a4-962c-7456e5399569</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='gender-male']</value>
      <webElementGuid>bd922fb0-1aad-47ad-806f-8990bac9f59d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/input</value>
      <webElementGuid>7550a9a3-ccc4-4208-89bf-6d438e55c571</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'gender-male' and @name = 'Gender' and @type = 'radio']</value>
      <webElementGuid>6397dcfb-81fa-43f3-b33d-cde57ef2f501</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
