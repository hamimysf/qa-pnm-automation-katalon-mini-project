<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_recovery_password</name>
   <tag></tag>
   <elementGuidId>7d11fd01-7404-4fbd-9759-8d61f0293f5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.page.password-recovery-page</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b7a09f3a-8679-40b7-84c3-26c2a7ceb5cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page password-recovery-page</value>
      <webElementGuid>dbd83ce8-cc6d-4800-aefc-1bfce57ae260</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        Password recovery
    
    
                    Please enter your email address below. You will receive a link to reset your password.
            
                
                    
                        Your email address:
                        
                        *
                        
                    
                
            
            
                
            
    
</value>
      <webElementGuid>1cbddc70-1440-47ea-8356-e3c57f0910ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page password-recovery-page&quot;]</value>
      <webElementGuid>c0714403-4f4a-43c0-84d3-8059330e6488</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[3]</value>
      <webElementGuid>9882ead3-3723-4f81-abd2-1107ba38a5fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[5]</value>
      <webElementGuid>8a663006-7129-4985-a48a-308ced0ebf9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div</value>
      <webElementGuid>665732b8-1080-48db-bd01-b463a7d93fd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        Password recovery
    
    
                    Please enter your email address below. You will receive a link to reset your password.
            
                
                    
                        Your email address:
                        
                        *
                        
                    
                
            
            
                
            
    
' or . = '
    
        Password recovery
    
    
                    Please enter your email address below. You will receive a link to reset your password.
            
                
                    
                        Your email address:
                        
                        *
                        
                    
                
            
            
                
            
    
')]</value>
      <webElementGuid>5cfe56ac-f21d-4dda-bf5a-b083cc423945</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
