<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recover_password_send_to_email</name>
   <tag></tag>
   <elementGuidId>8424ea1a-7aaf-46c3-abb9-b08d0a3d6a74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Password recovery'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.result</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>876378da-bd7a-4de9-ad3d-ad95159aadc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>result</value>
      <webElementGuid>1dc952ed-2d30-41d5-981c-4f7f096b1d39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Email with instructions has been sent to you.
            </value>
      <webElementGuid>7774ddb6-258c-4a3b-8f8b-0451242d31f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page password-recovery-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;result&quot;]</value>
      <webElementGuid>7aff676a-27af-4191-98c9-6a97e2da5b50</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password recovery'])[1]/following::div[2]</value>
      <webElementGuid>56fe3e3d-bc1f-421b-808c-46450fe3f953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[6]</value>
      <webElementGuid>1749fe40-1d6d-4b19-928d-3dbd45cc93c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your email address:'])[1]/preceding::div[1]</value>
      <webElementGuid>1342c51f-53c0-41a9-ab6c-2ee90e29619a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/preceding::div[1]</value>
      <webElementGuid>69d184de-5d92-4353-a80b-cfa0a25da312</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email with instructions has been sent to you.']/parent::*</value>
      <webElementGuid>b978da59-2285-41f2-8b69-448c8ef2ce46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div</value>
      <webElementGuid>102ff583-4ef7-4787-aba2-172d163f2853</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Email with instructions has been sent to you.
            ' or . = '
                Email with instructions has been sent to you.
            ')]</value>
      <webElementGuid>52845e74-99f8-43e6-bebd-d4849e1480c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
