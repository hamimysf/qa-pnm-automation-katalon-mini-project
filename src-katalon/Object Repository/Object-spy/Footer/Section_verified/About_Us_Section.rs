<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>About_Us_Section</name>
   <tag></tag>
   <elementGuidId>49705d00-38e6-4eb3-87da-4187c22338ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>06f847fb-7417-46c6-a7a8-245f3d3e8acb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-3</value>
      <webElementGuid>4a063ae7-4644-45a6-9e74-b06aa556dbfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        
            
                About Us
        
    
    
        Put your &quot;About Us&quot; information here. You can edit this in the admin site.
    


    
</value>
      <webElementGuid>c09b3dcd-9d48-40b7-953a-e2a1917d784d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]</value>
      <webElementGuid>fb1f0fe0-dfb9-47b2-9b6c-2e852a9fc9b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      <webElementGuid>9dc99ea6-f1dd-4d75-a8e6-6c6c50e6cff1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[3]</value>
      <webElementGuid>f7add65d-c473-4502-a8b4-dd022f597224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[5]</value>
      <webElementGuid>45e1e2ff-0696-49e6-9f14-083faa5e419a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[3]</value>
      <webElementGuid>8494135a-7a6e-4bfe-8bf6-204ee874cb9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        
            
                About Us
        
    
    
        Put your &quot;About Us&quot; information here. You can edit this in the admin site.
    


    
' or . = '
    
    

    
        
            
                About Us
        
    
    
        Put your &quot;About Us&quot; information here. You can edit this in the admin site.
    


    
')]</value>
      <webElementGuid>3a91f6a9-c47d-4852-8c85-3668eeff47b3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
