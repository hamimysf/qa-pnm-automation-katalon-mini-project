<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Contact_Us_Section</name>
   <tag></tag>
   <elementGuidId>5d2f44c5-19e8-4e9d-a536-b4c371ef4886</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.page.contact-page</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>83b50a07-3e69-46c8-8a18-2782de9bd3d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page contact-page</value>
      <webElementGuid>76f14fa1-1d3e-47ed-b660-36fa823d8731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        Contact Us
    
        
        
            Put your contact information here. You can edit this in the admin site.
        
    

    
        
            
                
            
            
                
                    
                        Your name
                        
                            
                        
                        
                    
                    
                        Your email
                        
                            
                        
                        
                    
                
                
                    Enquiry
                    
                        
                    
                    
                
            
            
                
            
        
    
</value>
      <webElementGuid>5eb48c15-fc99-4d47-b749-996f1cc8f9e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page contact-page&quot;]</value>
      <webElementGuid>1f0fe334-85c3-4799-97a2-1fd810d3d150</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[3]</value>
      <webElementGuid>98ef664a-4728-4561-bab3-af376be51c6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[5]</value>
      <webElementGuid>3f7a304b-4ca9-4f57-948c-bd254beb241f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div</value>
      <webElementGuid>a55b5a6e-23e7-4bad-bd65-314f7e7a0c85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        Contact Us
    
        
        
            Put your contact information here. You can edit this in the admin site.
        
    

    
        
            
                
            
            
                
                    
                        Your name
                        
                            
                        
                        
                    
                    
                        Your email
                        
                            
                        
                        
                    
                
                
                    Enquiry
                    
                        
                    
                    
                
            
            
                
            
        
    
' or . = '
    
        Contact Us
    
        
        
            Put your contact information here. You can edit this in the admin site.
        
    

    
        
            
                
            
            
                
                    
                        Your name
                        
                            
                        
                        
                    
                    
                        Your email
                        
                            
                        
                        
                    
                
                
                    Enquiry
                    
                        
                    
                    
                
            
            
                
            
        
    
')]</value>
      <webElementGuid>4d57b1b6-46a9-4074-9e75-9bc2882a863e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
