<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Sitemap_Section</name>
   <tag></tag>
   <elementGuidId>a043cfc2-6eaa-4321-8a9e-8c336ec69fad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.page.sitemap-page</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>05482b0d-5325-4f75-b020-fdbf5a5c6e5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page sitemap-page</value>
      <webElementGuid>02983f44-dd62-4994-bf49-8e6d2a6651ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        Sitemap
    
    
        
            
                
                    Search 
                        News
                                            Blog
                                        My account
                
            
        
            
                
                    Categories
                
                
                    
                            Books
                            Computers
                            Desktops
                            Notebooks
                            Accessories
                            Electronics
                            Camera, photo
                            Cell phones
                            Apparel &amp; Shoes
                            Digital downloads
                            Jewelry
                            Gift Cards
                    
                
            
                    
                
                    Manufacturers
                
                
                    
                            Tricentis
                    
                
            
            
</value>
      <webElementGuid>35c42863-3454-4d5e-9403-3f437b4a97d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page sitemap-page&quot;]</value>
      <webElementGuid>d506ffd6-ee68-4242-96b0-d91b1468598e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::div[6]</value>
      <webElementGuid>e69bbd32-6fa8-4c64-a9ef-d4a59e8d2a1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jewelry'])[2]/following::div[7]</value>
      <webElementGuid>5caddfc0-4e02-4983-a87b-b13394bbeffb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div</value>
      <webElementGuid>c8aef622-4947-4f4e-9345-a02bb12ed987</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        Sitemap
    
    
        
            
                
                    Search 
                        News
                                            Blog
                                        My account
                
            
        
            
                
                    Categories
                
                
                    
                            Books
                            Computers
                            Desktops
                            Notebooks
                            Accessories
                            Electronics
                            Camera, photo
                            Cell phones
                            Apparel &amp; Shoes
                            Digital downloads
                            Jewelry
                            Gift Cards
                    
                
            
                    
                
                    Manufacturers
                
                
                    
                            Tricentis
                    
                
            
            
' or . = '
    
        Sitemap
    
    
        
            
                
                    Search 
                        News
                                            Blog
                                        My account
                
            
        
            
                
                    Categories
                
                
                    
                            Books
                            Computers
                            Desktops
                            Notebooks
                            Accessories
                            Electronics
                            Camera, photo
                            Cell phones
                            Apparel &amp; Shoes
                            Digital downloads
                            Jewelry
                            Gift Cards
                    
                
            
                    
                
                    Manufacturers
                
                
                    
                            Tricentis
                    
                
            
            
')]</value>
      <webElementGuid>b48b1259-d56b-4b3b-aaf3-b7ebe691e8ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
