<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Privacy_Policy_Section</name>
   <tag></tag>
   <elementGuidId>37f646e3-3fb5-48b1-91dc-e583a5efbacd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>264fa4ee-f891-44f9-903e-a6e279802347</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-3</value>
      <webElementGuid>ef34ee2d-0518-42ca-a64b-934b56186b67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        
            
                Privacy policy
        
    
    
        Put your privacy policy information here. You can edit this in the admin site.
    


    
</value>
      <webElementGuid>67fe8c56-9f82-4b34-bbce-c5e9d68e1e2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]</value>
      <webElementGuid>ee6660f3-ad37-4d25-843c-66eb12b231fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      <webElementGuid>1a314689-f2ff-40c9-939c-403b9b605419</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[3]</value>
      <webElementGuid>6d00800b-851b-4da8-a02c-fec9bbe29bd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[5]</value>
      <webElementGuid>31d43ded-f314-4523-8694-a7a5f39b1cdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[3]</value>
      <webElementGuid>51383a4a-0277-4b9a-9f2f-9842a17fcc8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        
            
                Privacy policy
        
    
    
        Put your privacy policy information here. You can edit this in the admin site.
    


    
' or . = '
    
    

    
        
            
                Privacy policy
        
    
    
        Put your privacy policy information here. You can edit this in the admin site.
    


    
')]</value>
      <webElementGuid>de56f5b7-3875-4e50-8d49-e9d9d6314255</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
