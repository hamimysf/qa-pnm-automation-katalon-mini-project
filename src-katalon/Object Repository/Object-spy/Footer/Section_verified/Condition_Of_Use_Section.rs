<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Condition_Of_Use_Section</name>
   <tag></tag>
   <elementGuidId>67e2e1e8-571d-41fb-b1bc-90b6187d910d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>669608c8-0811-4a62-b775-92d6e681430c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-3</value>
      <webElementGuid>315b0908-341a-45cf-ab37-0274c17eafb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        
            
                Conditions of use
        
    
    
        Put your conditions of use information here. You can edit this in the admin site.
    


    
</value>
      <webElementGuid>afdf6130-bcd2-4712-8c88-d5220c60de1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]</value>
      <webElementGuid>6aa52afe-e39d-41c3-92bf-fa46aa68d375</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      <webElementGuid>874cc925-3a0e-4e81-b1a0-1117e4dcf15b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[3]</value>
      <webElementGuid>0c83c00a-6809-4c73-ae5a-53764cd2192f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[5]</value>
      <webElementGuid>fb57270c-0f2f-49fa-aa50-148bc6f6e6e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[3]</value>
      <webElementGuid>9d9891a1-5f2f-4ab8-9c72-979ae6365802</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        
            
                Conditions of use
        
    
    
        Put your conditions of use information here. You can edit this in the admin site.
    


    
' or . = '
    
    

    
        
            
                Conditions of use
        
    
    
        Put your conditions of use information here. You can edit this in the admin site.
    


    
')]</value>
      <webElementGuid>049af8a8-af0d-4ee9-9b29-945ac24ae002</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
