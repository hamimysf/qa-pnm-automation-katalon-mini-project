<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search_Section</name>
   <tag></tag>
   <elementGuidId>d775efff-b837-4b8d-9cb7-91e2ae29656a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ee5d46fa-551a-45b2-bb37-b0cdd5fdaf7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-2</value>
      <webElementGuid>334b415f-d6d2-4b7f-b4e3-84e9e3e98450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($('#As').is(':checked')) {
            $('#advanced-search-block').show();
        }
        else {
            $('#advanced-search-block').hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                                
        
                                    
                
            
        
    


    
</value>
      <webElementGuid>2055f045-bef5-4186-937b-3713a03a74aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]</value>
      <webElementGuid>b9040e83-94ba-4d01-b601-7cb6aebed6e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      <webElementGuid>1b56141c-655c-48ff-a99b-e7d04cf57bb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[4]</value>
      <webElementGuid>81b513fa-ee74-4a57-9f98-ede5952a2e56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>c85d6fd6-1d41-4302-af60-edd0b2d2be5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($(&quot; , &quot;'&quot; , &quot;#As&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).show();
        }
        else {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                                
        
                                    
                
            
        
    


    
&quot;) or . = concat(&quot;
    
    


    $(document).ready(function () {

        $(&quot;#As&quot;).click(toggleAdvancedSearch);

        toggleAdvancedSearch();
    });

    function toggleAdvancedSearch() {

        if ($(&quot; , &quot;'&quot; , &quot;#As&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).show();
        }
        else {
            $(&quot; , &quot;'&quot; , &quot;#advanced-search-block&quot; , &quot;'&quot; , &quot;).hide();
        }
    }


    
        Search
    
    
        
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        
        
            
                                
        
                                    
                
            
        
    


    
&quot;))]</value>
      <webElementGuid>b8f13da6-d45a-4ad9-93a1-fad318d70be4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
