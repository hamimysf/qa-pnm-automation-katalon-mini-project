<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Shipping_return_Section</name>
   <tag></tag>
   <elementGuidId>c52ab8ef-ef65-4247-8f9a-88c2c0af4c72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6eb47738-d8b5-4f06-87fd-5d597565bc49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-3</value>
      <webElementGuid>c561313d-fbc4-4f51-ad3a-08831f54706b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        
            
                Shipping &amp; Returns
        
    
    
        Put your shipping &amp; returns information here. You can edit this in the admin site.
    


    
</value>
      <webElementGuid>1ce5aa18-492b-4903-9db2-8df6fa5ba4eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]</value>
      <webElementGuid>812d9593-659e-4357-9dc1-e44d6b264964</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[2]/following::div[2]</value>
      <webElementGuid>1f601e9a-5922-4b46-b2b1-93d72c2de4dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very bad'])[1]/following::div[3]</value>
      <webElementGuid>98575b78-bbd5-4111-962a-d5edfbbbcc63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[5]</value>
      <webElementGuid>a0d6a950-c5a7-4ca6-926f-fc8fc7a861e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[3]</value>
      <webElementGuid>94841379-b82c-4d03-ac49-f215626d6d8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        
            
                Shipping &amp; Returns
        
    
    
        Put your shipping &amp; returns information here. You can edit this in the admin site.
    


    
' or . = '
    
    

    
        
            
                Shipping &amp; Returns
        
    
    
        Put your shipping &amp; returns information here. You can edit this in the admin site.
    


    
')]</value>
      <webElementGuid>477c1a43-bfe5-425b-affb-8018ad771966</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
