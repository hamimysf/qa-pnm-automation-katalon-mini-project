<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>New_Products_Section</name>
   <tag></tag>
   <elementGuidId>eee3ff02-5be5-4bba-8778-9291d3f2a1a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.center-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1aca071f-1602-41fa-bb1a-c0025461f741</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-2</value>
      <webElementGuid>658417c2-aade-40f7-a59e-52f30c0b3711</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        RSS
        New products
    
    
            
                   
                       

    
        
            
        
    
    
        
            Phone Cover
        
        
            Phone Cover for Samsung and Apple models.
Available in 4 different colors: Black, White, Blue and Yellow.
        
        
            
                10.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Copy of Computing and Internet EX
        
            
                
                    
                    
                
            
        
            More Than 100 tips about computing and internet.
        
        
            
                    30.00
                10.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Coffee Capsule - Cappucino
        
            
                
                    
                    
                
            
        
            La
        
        
            
                
            
            
                
            
            
        
    


                   
                    
    


    
</value>
      <webElementGuid>651c67ea-7d23-42c8-b6f6-3c8a45670e27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]</value>
      <webElementGuid>b8116160-9eac-455b-889b-98799ee79ee2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      <webElementGuid>a84bda47-1a37-45f6-adb8-09f7d9ac7e6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[4]</value>
      <webElementGuid>7c4e7aa3-7268-4769-80fe-ade506d8d44a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>0d059d66-dbdb-41ec-8af4-fdcc4cbd3dbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        RSS
        New products
    
    
            
                   
                       

    
        
            
        
    
    
        
            Phone Cover
        
        
            Phone Cover for Samsung and Apple models.
Available in 4 different colors: Black, White, Blue and Yellow.
        
        
            
                10.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Copy of Computing and Internet EX
        
            
                
                    
                    
                
            
        
            More Than 100 tips about computing and internet.
        
        
            
                    30.00
                10.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Coffee Capsule - Cappucino
        
            
                
                    
                    
                
            
        
            La
        
        
            
                
            
            
                
            
            
        
    


                   
                    
    


    
' or . = '
    
    

    
        RSS
        New products
    
    
            
                   
                       

    
        
            
        
    
    
        
            Phone Cover
        
        
            Phone Cover for Samsung and Apple models.
Available in 4 different colors: Black, White, Blue and Yellow.
        
        
            
                10.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Copy of Computing and Internet EX
        
            
                
                    
                    
                
            
        
            More Than 100 tips about computing and internet.
        
        
            
                    30.00
                10.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Simple Computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own expensive computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                1800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Create Your Own Jewelry
        
            
                
                    
                    
                
            
        
            The best Jewelry for the creative girl of today!
        
        
            
                100.00
            
            
                
                    
            
            
        
    


                   
                   
                       

    
        
            
        
    
    
        
            Coffee Capsule - Cappucino
        
            
                
                    
                    
                
            
        
            La
        
        
            
                
            
            
                
            
            
        
    


                   
                    
    


    
')]</value>
      <webElementGuid>34b21743-0448-4566-a341-2fa4eb58fd5a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
