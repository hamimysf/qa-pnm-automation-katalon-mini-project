<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_Shopping cart</name>
   <tag></tag>
   <elementGuidId>2216a8de-0070-4469-aa1b-2a829fe49454</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Shopping cart')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>03b67759-9ca3-4aba-8e97-90567147e5ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/cart</value>
      <webElementGuid>72d0601c-93c5-4d7a-babd-8099f5d29f59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ico-cart</value>
      <webElementGuid>836bdaf9-1c75-4c2d-a834-c60a1735b25e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shopping cart</value>
      <webElementGuid>9e102cba-0a99-40c7-abca-a1d4615d1d54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column my-account&quot;]/ul[1]/li[4]/a[@class=&quot;ico-cart&quot;]</value>
      <webElementGuid>8f517465-6daf-4dcb-a449-e35ad0c5d55f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Shopping cart')]</value>
      <webElementGuid>a6bcae1e-61ff-496f-9771-71b15d07fa9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Addresses'])[1]/following::a[1]</value>
      <webElementGuid>8301b952-5aba-44ef-927a-b06488365fca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Orders'])[1]/following::a[2]</value>
      <webElementGuid>0760eea5-049f-4284-a863-b502c5fa1f6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wishlist'])[2]/preceding::a[1]</value>
      <webElementGuid>e9b2f746-f86c-491f-a7e2-378f74b9f008</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Follow us'])[1]/preceding::a[2]</value>
      <webElementGuid>a40e491b-1a90-4b5a-b05f-1b2ec710cfd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/cart')])[2]</value>
      <webElementGuid>76c0afbc-e6c3-4cb1-baa2-5ce7ea6172ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/ul/li[4]/a</value>
      <webElementGuid>7a9fd22d-9f8c-4ae2-9267-3a65782b70a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/cart' and (text() = 'Shopping cart' or . = 'Shopping cart')]</value>
      <webElementGuid>1701398d-cb56-49e9-9818-fc38cfdb205e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
