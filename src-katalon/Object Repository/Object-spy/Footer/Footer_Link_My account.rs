<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_My account</name>
   <tag></tag>
   <elementGuidId>d90a1816-1e57-41aa-aff5-2be32fdf6247</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'My account')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.account</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ddbfc244-be46-40de-aadd-05fbd8296eb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/customer/info</value>
      <webElementGuid>669597dd-9911-4051-9bdb-d1849c2142fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>account</value>
      <webElementGuid>71ad305a-4f0c-44c6-b311-f4c3e421b852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My account</value>
      <webElementGuid>9b17fe02-1276-4935-82f9-31d9fb8cbc2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column my-account&quot;]/ul[1]/li[1]/a[@class=&quot;account&quot;]</value>
      <webElementGuid>6c8f9711-abfb-4169-8553-6aec614ce057</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'My account')]</value>
      <webElementGuid>174a1778-a663-4aaa-a0ac-d4386e73e7f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My account'])[1]/following::a[1]</value>
      <webElementGuid>4da4731b-b9cc-429a-b8f5-66cb7151c8da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New products'])[1]/following::a[1]</value>
      <webElementGuid>78f5be53-9bb2-455f-ac00-751cb5595202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Orders'])[1]/preceding::a[1]</value>
      <webElementGuid>94a288fc-6d1e-4a77-a75e-e8b01495381e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Addresses'])[1]/preceding::a[2]</value>
      <webElementGuid>e54e4e99-fb25-4a00-831e-a992bf6775e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/customer/info')]</value>
      <webElementGuid>67ef3137-0c1f-40fc-9d91-153904dc1d62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/ul/li/a</value>
      <webElementGuid>052ccc7d-34be-48be-9983-9b3c1937ff13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/customer/info' and (text() = 'My account' or . = 'My account')]</value>
      <webElementGuid>4189b016-2862-4cfa-8f42-2b2ec5354958</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
