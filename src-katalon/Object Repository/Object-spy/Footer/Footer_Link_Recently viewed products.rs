<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_Recently viewed products</name>
   <tag></tag>
   <elementGuidId>37838767-0472-4fab-8f13-92f43748a353</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Recently viewed products')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ca9d40e9-79ea-43ee-a65a-de250c0e2983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/recentlyviewedproducts</value>
      <webElementGuid>cdcb2925-cc4a-47da-91f3-5e836126c6e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Recently viewed products</value>
      <webElementGuid>134bff36-d0aa-481c-aa41-9f4ec17f4fdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column customer-service&quot;]/ul[1]/li[4]/a[1]</value>
      <webElementGuid>b0f6e291-e443-4389-974c-dc2fef27df00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Recently viewed products')]</value>
      <webElementGuid>5bb7d6d7-10b3-4f07-bfa7-1d19601a3de5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::a[1]</value>
      <webElementGuid>674d4a2c-6bab-4234-981c-dff1871fe51d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='News'])[1]/following::a[2]</value>
      <webElementGuid>33267e6c-3ed0-44bf-bccf-04c12aa5661f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare products list'])[1]/preceding::a[1]</value>
      <webElementGuid>8b87a435-dec4-450c-aa62-eac9d3263dd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New products'])[1]/preceding::a[2]</value>
      <webElementGuid>9d0ef9cf-52b2-4e35-bc3b-8cd110a5fb06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/recentlyviewedproducts')]</value>
      <webElementGuid>f4aa25b1-da2a-45b4-9158-c2ab5c5eb856</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/ul/li[4]/a</value>
      <webElementGuid>49561620-5410-427b-ab49-7f354f05c4fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/recentlyviewedproducts' and (text() = 'Recently viewed products' or . = 'Recently viewed products')]</value>
      <webElementGuid>2fce8ca7-c42b-4963-a48a-37c98a31707b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
