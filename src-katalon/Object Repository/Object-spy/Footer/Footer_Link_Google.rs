<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_Google</name>
   <tag></tag>
   <elementGuidId>8f1773df-d75a-4645-882e-713f11366281</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Google+')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.google-plus > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7478d022-6808-411e-92f2-aa95785be54c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://plus.google.com/+nopcommerce</value>
      <webElementGuid>28a50965-0436-4323-8724-e77b8a97ee2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>db909d5a-f1e1-46b6-ac17-55f18af6abb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Google+</value>
      <webElementGuid>85ab7729-1613-4df2-a685-9cdf578bb4ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column follow-us&quot;]/ul[1]/li[@class=&quot;google-plus&quot;]/a[1]</value>
      <webElementGuid>5305554e-71dc-4ef2-aefe-bf4fe403961e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Google+')]</value>
      <webElementGuid>779fa065-6357-4d50-a743-96ba99c8353c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='YouTube'])[1]/following::a[1]</value>
      <webElementGuid>ac16b5c1-af2d-4230-98c7-80e716e77d3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RSS'])[1]/following::a[2]</value>
      <webElementGuid>effe4937-f1ad-44a4-b08f-5cba52dbbdbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='nopCommerce'])[1]/preceding::a[1]</value>
      <webElementGuid>ae7aefeb-5c43-498c-b799-2a7f25973738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://plus.google.com/+nopcommerce')]</value>
      <webElementGuid>ab61007e-dfcf-4d91-8177-d21280c6ded1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/ul/li[5]/a</value>
      <webElementGuid>640853dd-a7af-4d85-861e-f891f69a175a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://plus.google.com/+nopcommerce' and (text() = 'Google+' or . = 'Google+')]</value>
      <webElementGuid>2a7f9239-ebaa-4d62-ae21-f1014f0c2991</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
