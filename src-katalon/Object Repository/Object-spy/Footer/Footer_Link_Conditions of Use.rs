<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_Conditions of Use</name>
   <tag></tag>
   <elementGuidId>9b68929c-a3d2-456f-bf8d-a450927b2fdf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Conditions of Use')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3b05305f-4fac-43c3-b05b-4d5a156a0ae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/conditions-of-use</value>
      <webElementGuid>1c4f8d34-0f51-4a03-8e1f-07d8c040b5e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Conditions of Use</value>
      <webElementGuid>f30935f8-36e5-416e-80fd-5501bb8ad977</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column information&quot;]/ul[1]/li[4]/a[1]</value>
      <webElementGuid>8f89879b-0dc9-49cc-8bfe-cf8be44ba437</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Conditions of Use')]</value>
      <webElementGuid>cf9dd53d-a817-42e4-90af-9a86e4e96e7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Notice'])[1]/following::a[1]</value>
      <webElementGuid>6826867e-bd8e-42ea-a820-7171f945608a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping &amp; Returns'])[1]/following::a[2]</value>
      <webElementGuid>6fffa52c-d3ce-4e2e-8f68-ada1ef0742ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/preceding::a[1]</value>
      <webElementGuid>512774b5-fd8e-4e44-9104-b6b0a67396ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact us'])[1]/preceding::a[2]</value>
      <webElementGuid>53d27ed4-08ef-45aa-a56a-acb47829e86f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/conditions-of-use')]</value>
      <webElementGuid>f70c0127-fd07-4848-b9b8-e43a8ae33f51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li[4]/a</value>
      <webElementGuid>2f895789-025e-416d-8f5f-17b357ac13f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/conditions-of-use' and (text() = 'Conditions of Use' or . = 'Conditions of Use')]</value>
      <webElementGuid>8a7f1375-0cff-40a3-8353-176e92741da3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
