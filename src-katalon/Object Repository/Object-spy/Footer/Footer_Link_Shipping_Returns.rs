<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Footer_Link_Shipping_Returns</name>
   <tag></tag>
   <elementGuidId>68bb58a7-8c84-41d0-bd5c-248edf659d9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Shipping &amp; Returns')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4a06a714-b0f5-49b4-9ca7-e950b3dff49d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/shipping-returns</value>
      <webElementGuid>0df7103d-3010-41f9-834e-32148ee3b4b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shipping &amp; Returns</value>
      <webElementGuid>386e0388-317f-411c-97ef-7b637d198c95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;footer&quot;]/div[@class=&quot;footer-menu-wrapper&quot;]/div[@class=&quot;column information&quot;]/ul[1]/li[2]/a[1]</value>
      <webElementGuid>c42c0c5e-4ad6-465c-8c01-298d8a8d41e4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Shipping &amp; Returns')]</value>
      <webElementGuid>28d48980-ca4b-4474-ab54-f4a4f1abb5a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/following::a[1]</value>
      <webElementGuid>9a82db4c-eedd-4818-add0-a1697e7cb8d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::a[2]</value>
      <webElementGuid>0a777770-3078-451b-baf3-226c082917aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Notice'])[1]/preceding::a[1]</value>
      <webElementGuid>7a988314-3de4-4a2f-b09a-0388e412cc7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Conditions of Use'])[1]/preceding::a[2]</value>
      <webElementGuid>583a7ed6-efd8-410a-9de0-37c9497194da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/shipping-returns')]</value>
      <webElementGuid>6a5867a2-7d54-4d41-8829-76d5c871a880</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li[2]/a</value>
      <webElementGuid>92735780-59ef-4e62-a68c-f30697775343</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/shipping-returns' and (text() = 'Shipping &amp; Returns' or . = 'Shipping &amp; Returns')]</value>
      <webElementGuid>57d8f2b4-3f7d-4a27-bbeb-b459ad9bc7fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
